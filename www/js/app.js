// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('inc', ['ionic','ngCordovaOauth','ngCordova','inc.controllers','inc.services','uiGmapgoogle-maps','ngSanitize','ngOpenFB','ngMap','yaru22.angular-timeago'])

.run(function($ionicPlatform, $location, ngFB, $rootScope, $location) {
	$ionicPlatform.ready(function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
        }
    		 
    	if(window.cordova && window.cordova.plugins.Keyboard) {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
	
			// Don't remove this line unless you know what you are doing. It stops the viewport
			// from snapping when text inputs are focused. Ionic handles this internally for
			// a much nicer keyboard experience.
			cordova.plugins.Keyboard.disableScroll(true);
	    }
	    if(window.StatusBar) {
	    	StatusBar.styleDefault();
	    }
	});
	
	var port = $location.$$port;
  	if (port == 80) { port = ''; }
  	else { port = ':' + port; }
  	
  	ngFB.init({appId: '204480250064720'});
  	
  	$rootScope.showTabs = true;
	if(ionic.Platform.isAndroid()){
		window.addEventListener('native.keyboardshow', keyboardShowHandler);
		window.addEventListener('native.keyboardhide', keyboardHideHandler);
	
		function keyboardShowHandler(e)
		{
			document.body.classList.add('keyboard-is-open');
		}
	
		function keyboardHideHandler(e)
		{
			document.body.classList.remove('keyboard-is-open');
		}
	}
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider, uiGmapGoogleMapApiProvider, $sceProvider, $locationProvider) {
    
	$stateProvider

	.state('welcome', {
    	url: '/welcome',
		templateUrl: 'templates/general/welcome.html',
        controller: 'WelcomeCtrl'
  	})
  	
  	.state('login', {
    	url: '/login',
		templateUrl: 'templates/general/login.html',
        controller: 'LoginCtrl'
  	})
  	
  	.state('response', {
    	url: '/response',
		templateUrl: 'oauthcallback.html',
        controller: 'ResponseCtrl'
  	})
  	
  	.state('forgot', {
    	url: '/forgot',
		templateUrl: 'templates/general/forgot.html',
        controller: 'ForgotCtrl'
  	})
  	
  	.state('reset', {
    	url: '/reset/:userId/:token',
		templateUrl: 'templates/general/reset.html',
        controller: 'ResetCtrl'
  	})
  	
  	.state('verify', {
    	url: '/verify/:userId/:token',
		templateUrl: 'templates/general/verify.html',
        controller: 'VerifyCtrl'
  	})

  	.state('register', {
    	url: '/register',
		templateUrl: 'templates/general/register.html',
        controller: 'RegisterCtrl'
  	})
  	
  	.state('dashboard', {
    	url: '/dashboard',
		templateUrl: 'templates/dashboard/index.html',
        controller: 'DashboardCtrl'
  	})
  	
  	.state('intro', {
    	url: '/intro',
		templateUrl: 'templates/dashboard/intro.html',
        controller: 'IntroCtrl'
  	})
  	
  	.state('dashboardMenu', {
    	url: '/dashboardmenu',
		templateUrl: 'templates/dashboard/index.html',
        controller: 'DashboardCtrlMenu'
  	})
  	
  	.state('newComplaint', {
	  	url: '/newcomplaint',
	  	templateUrl: 'templates/complaint/new.html',
	  	controller: 'NewComplaintCtrl'
  	})
  	
  	.state('titleComplaint', {
	  	url: '/titlecomplaint',
	  	templateUrl: 'templates/complaint/title.html',
	  	controller: 'TitleComplaintCtrl'
  	})
  	
  	.state('MyComplaints', {
	  	url: '/mycomplaints',
	  	templateUrl: 'templates/complaint/my.html',
	  	controller: 'MyComplaintsCtrl'
  	})
  	
  	.state('ComplaintsDependency', {
	  	url: '/complaintsdependency',
	  	templateUrl: 'templates/complaint/complaints_dependency.html',
	  	controller: 'ComplaintsDependencyCtrl'
  	})
  	
  	.state('ViewComplaint', {
	  	url: '/viewcomplaint/:complaintId',
	  	templateUrl: 'templates/complaint/view.html',
	  	controller: 'ViewComplaintCtrl'
  	})
  	
  	.state('ViewComplaintFollow', {
	  	url: '/viewcomplaintfollow',
	  	templateUrl: 'templates/complaint/view.html',
	  	controller: 'ViewComplaintFollowCtrl'
  	})
  	
  	.state('AddCommentComplaint', {
	  	url: '/addcommentcomplaint/:complaintId',
	  	templateUrl: 'templates/complaint/comment.html',
	  	controller: 'AddCommentComplaintCtrl'
  	})
  	
  	.state('AddCommentComplaintJoin', {
	  	url: '/addcommentcomplaintjoin',
	  	templateUrl: 'templates/complaint/comment.html',
	  	controller: 'AddCommentComplaintJoinCtrl'
  	})
  	
  	.state('blog', {
	  	url: '/blog',
	  	templateUrl: 'templates/blog/index.html',
	  	controller: 'BlogCtrl'
  	})
  	
  	.state('news', {
	  	url: '/news',
	  	templateUrl: 'templates/news/index.html',
	  	controller: 'NewsCtrl'
  	})
  	
  	.state('newsSingle', {
	  	url: '/news/single',
	  	templateUrl: 'templates/news/single.html',
	  	controller: 'NewsSingleCtrl'
  	})
  	
  	.state('CategoryComplaint', {
		url: '/categorycomplaint',
		templateUrl: 'templates/complaint/category.html',
		controller: 'CategoryComplaintCtrl'	
  	})
  	
  	.state('AmountComplaint', {
		url: '/amountcomplaint',
		templateUrl: 'templates/complaint/amount.html',
		controller: 'AmountComplaintCtrl'	
  	})
  	
  	.state('VerifyComplaint', {
		url: '/verifycomplaint',
		templateUrl: 'templates/complaint/verify.html',
		controller: 'VerifyComplaintCtrl'	
  	})
  	
  	.state('VerifyComplaintSend', {
		url: '/verifycomplaintsend',
		templateUrl: 'templates/complaint/verify.html',
		controller: 'VerifyComplaintSendCtrl'	
  	})
  	
  	.state('newGroupComplaints', {
		url: '/newgroupcomplaints',
		templateUrl: 'templates/complaint/newGroup.html',
		controller: 'NewGroupComplaintsCtrl'	
  	})
  	
  	.state('GroupsComplaints', {
		url: '/groupscomplaints',
		templateUrl: 'templates/complaint/groups.html',
		controller: 'GroupsComplaintsCtrl'	
  	})
  	
  	.state('GroupsDependency', {
		url: '/groupsdependency',
		templateUrl: 'templates/complaint/groups_dependency.html',
		controller: 'GroupsDependencyCtrl'	
  	})
  	
  	.state('ComplaintsGroupDependency', {
		url: '/complaintsgroupdependency',
		templateUrl: 'templates/complaint/new_groups_dependency.html',
		controller: 'ComplaintsGroupDependencyCtrl'	
  	})
  	
  	.state('ViewComplaintGroup', {
	  	url: '/viewcomplaintgroup/:groupId',
	  	templateUrl: 'templates/complaint/viewcomplaintgroup.html',
	  	controller: 'ViewComplaintGroupCtrl'
  	})
  	
  	.state('NewComplaints', {
		url: '/newcomplaints',
		templateUrl: 'templates/complaint/newComplaints.html',
		controller: 'NewComplaintsCtrl'	
  	})
  	
  	.state('ViewGroup', {
	  	url: '/viewgroup/:groupId',
	  	templateUrl: 'templates/complaint/viewgroup.html',
	  	controller: 'ViewGroupCtrl'
  	})
  	
  	.state('Profile', {
	  	url: '/profile',
	  	templateUrl: 'templates/profile/index.html',
	  	controller: 'ProfileCtrl'
  	})
  	
  	.state('MyNotifications', {
	  	url: '/mynotifications',
	  	templateUrl: 'templates/notifications/index.html',
	  	controller: 'MyNotificationsCtrl'
  	})
  	
  	/* LOGOUT */
  	.state('logout', {
    	url: '/logout',
    	templateUrl: 'templates/general/logout.html',
		controller: 'LogoutCtrl'
  	})
  	
  	$urlRouterProvider.otherwise("/welcome");

  	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);

  	//Tabs al fondo
  	$ionicConfigProvider.platform.android.tabs.position('bottom');

  	//Alinear Titulo en Android
  	$ionicConfigProvider.platform.android.navBar.alignTitle('center');

  	//Alinear Botones en Android
  	$ionicConfigProvider.platform.android.navBar.positionPrimaryButtons('left');
  	$ionicConfigProvider.platform.android.navBar.positionSecondaryButtons('right');

  	//Android style striped tabs
  	$ionicConfigProvider.platform.android.tabs.style('standard');

  	uiGmapGoogleMapApiProvider.configure({
		key: 'AIzaSyCdONyfCadeDvw20F1Q03eJj80WnGmCZ4g',
		v: '3.20',
		libraries: 'places,geometry,visualization',
		language: 'es',
		sensor: 'true',
		china: true
	});
	
	$sceProvider.enabled(false);
	
})

.directive('ngFileSelect', [ '$parse', '$timeout', function($parse, $timeout) {
return function(scope, elem, attr) {
        var fn = $parse(attr['ngFileSelect']);
        elem.bind('change', function(evt) {
            var files = [], fileList, i;
            fileList = evt.target.files;
            if (fileList != null) {
                for (i = 0; i < fileList.length; i++) {
                    files.push(fileList.item(i));
                }
            }
            $timeout(function() {
                fn(scope, {
                    $files : files,
                    $event : evt
                });
            });
        });
    };
}])

.directive('showWhen', ['$window', function($window) {
 return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

	  function checkExpose() {
		var mq = $attr.showWhen == 'large' ? '(min-width:768px)' : $attr.showWhen;
		if($window.matchMedia(mq).matches){
			$element.removeClass('ng-hide');
		} else {
			$element.addClass('ng-hide');		
		}
	  }

	  function onResize() {
		debouncedCheck();
	  }

	  var debouncedCheck = ionic.debounce(function() {
		$scope.$apply(function(){
		  checkExpose();
		});
	  }, 300, false);

	  checkExpose();

	  ionic.on('resize', onResize, $window);

	  $scope.$on('$destroy', function(){
		ionic.off('resize', onResize, $window);
	  });

	}
  };
}])

.directive('hideWhen', ['$window', function($window) {
 return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

	  function checkExpose() {
		var mq = $attr.hideWhen == 'large' ? '(min-width:768px)' : $attr.hideWhen;
		if($window.matchMedia(mq).matches){
			$element.addClass('ng-hide');
		} else {
			$element.removeClass('ng-hide');		
		}
	  }

	  function onResize() {
		debouncedCheck();
	  }

	  var debouncedCheck = ionic.debounce(function() {
		$scope.$apply(function(){
		  checkExpose();
		});
	  }, 300, false);

	  checkExpose();

	  ionic.on('resize', onResize, $window);

	  $scope.$on('$destroy', function(){
		ionic.off('resize', onResize, $window);
	  });

	}
  };
}])

.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
})

.directive('compile', ['$compile', function ($compile) {
      return function(scope, element, attrs) {
	      scope.$watch(
            function(scope) {
               // watch the 'compile' expression for changes
              return scope.$eval(attrs.compile);
            },
            function(value) {
              // when the 'compile' expression changes
              // assign it into the current DOM
              element.html(value);

              // compile the new DOM and link it to the current
              // scope.
              // NOTE: we only compile .childNodes so that
              // we don't get into infinite loop compiling ourselves
              $compile(element.contents())(scope);
            }
        );
    };
}])
