angular.module('inc.controllers', [])

.controller('WelcomeCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout) {

	var uuid;
	
	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Welcome');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			//Redirect To Home App
			$state.go('dashboard');
		}
	});
	
	//Open Register View
	$scope.register = function(event) {
		$state.go('register');
	}

	//Open Login View
	$scope.login = function(event) {
		$state.go('login');
	}
})

.controller('LoginCtrl', function($scope, $state, $stateParams, $location, $cordovaOauth, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $ionicPopup, ngFB) {

	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Login');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			//Redirect To Home App
			$state.go('dashboard');
		}
		
		$scope.login = {
		    name : '',
		    password : ''
		};
	});
	
	//Open Register View
	$scope.register = function(event) {
		$state.go('register');
	}
	
	//Do Login View
	$scope.doLogin = function(login) {
		
		//Read Values
		var name = login.name;
		var password = login.password;
		
		//Check Values
		if (name && password)
		{
			//Send API Call
            API.doLogin(name,password)
			.success(function(data) {
				console.log(data);
				if (data.status)
				{
					if (data.data.status === '0')
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Aún no has verificado tu correo electrónico para poder entrar a Incorruptible.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					else
					{
						$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
						$state.go('dashboard');
					}
				}
				else
				{
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				}
			})
			.error(function(data) {
				console.log(data);
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de comunicación con el servidor.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Llena todos los campos.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
	//facebookLogin 
	$scope.facebookLogin = function(event) {
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			$cordovaOauth.facebook("204480250064720", ["email,public_profile"]).then(function(result) {
	        	var token = result.access_token;
	        	console.log(token);
	        	
				$ionicLoading.show({
			    	template: 'Procesando...',
					noBackdrop: false
			    });
			
				//Send API Call
	            API.loginFacebook(token)
				.success(function(data) {
					$ionicLoading.hide();
					if (data.status)
					{
						if (data.data.status === '0')
						{
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Aún no has verificado tu correo electrónico para poder entrar a Incorruptible.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						else
						{
							$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
							$state.go('dashboard');
						}
					}
					else
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error al procesar la información (API Status).</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
				})
				.error(function(data) {
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de conexión con el server (API 500).</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
						
	        }, function(error) {
		        console.log(error);
	        });
	   }
	   else
	   {
		   	ngFB.login({scope: 'email,public_profile'}).then(
		   	function (response) {
            	if (response.status === 'connected') {
                	console.log('Facebook login succeeded');
                	
                	var token = response.authResponse.accessToken;
		        	console.log(token);
		        	
					$ionicLoading.show({
				    	template: 'Procesando...',
						noBackdrop: false
				    });
				
					//Send API Call
		            API.loginFacebook(token)
					.success(function(data) {
						$ionicLoading.hide();
						if (data.status)
						{
							if (data.data.status === '0')
							{
								var alertPopup = $ionicPopup.alert({
								    title: 'Incorruptible',
								    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Aún no has verificado tu correo electrónico para poder entrar a Incorruptible.</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
							}
							else
							{
								$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
								$state.go('dashboard');
							}
						}
						else
						{
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error al procesar la información (API Status).</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
					})
					.error(function(data) {
						$ionicLoading.hide();
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de conexión con el server (API 500).</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
                	 
				} else {
                	var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Proceso de Inicio de Sesión cancelado.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				}
			});
	   }
	}
	
	//twitterLogin 
	$scope.twitterLogin = function(event) {
		var clientId = "gya0r31yP0565qMefn4QImAIx";
		var clientSecret = "7s4XvvTQkGhloVS9YB9Ux08R4z3SLhYojOFsHnGdYLAJ1ZtKGi";
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
			
		if (isIOS || isAndroid)
		{		
			$cordovaOauth.twitter(clientId,clientSecret, {redirect_uri: "http://10.0.2.2/callback"}).then(function(result){
				var token = result.oauth_token;
	        	var secret = result.oauth_token_secret;
	        	var id = result.user_id;
	        	var name = result.screen_name;
	        	
	        	$ionicLoading.show({
			      template: 'Procesando...',
			      duration: 3000
			    }).then(function(){
			       console.log("The loading indicator is now displayed");
			    });
			    
	        	API.getTwitterProfile(clientId,clientSecret,token,secret);
	        	$state.go('dashboard');
						
	        }, function(error) {
		        console.log(error);
	        });
	    }
	    else
	    {
		    hello.init({'twitter' : clientId});
		    
		    // Twitter instance
			var twitter = hello('twitter');
			var access_token = '';
			// Login
			twitter.login().then( function(r){
				
				access_token = r.authResponse.access_token;
				
				// Get Profile
				return twitter.api('account/verify_credentials.json?include_email=true');
				
			}, log )
			.then( function(p){
				
				$ionicLoading.show({
			    	template: 'Procesando...',
					noBackdrop: false
			    });
				
				//Send API Call
	            API.doLoginTwitter(p.email, p.screen_name, p.id_str, access_token, p.profile_image_url)
				.success(function(data) {
					if (data.status)
					{
						if (data.data.status === '0')
						{
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Aún no has verificado tu correo electrónico para poder entrar a Incorruptible.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						else
						{
							$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
							$state.go('dashboard');
						}
						$ionicLoading.hide();
					}
					else
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
						$ionicLoading.hide();
					}
				})
				.error(function(data) {
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de comunicación con el servidor.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
					$ionicLoading.hide();
				});
			}, log );
	    }
	}
	
	//goForgot
	$scope.forgot = function(event) {
		$state.go('forgot');
	}
	
	function log(){
		console.log(arguments);
	}
})

.controller('ResponseCtrl', function($scope, $state, $stateParams, $location, $cordovaOauth, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $ionicPopup) { })

.controller('RegisterCtrl', function($scope, $state, $stateParams, $location, $cordovaOauth, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $ionicPopup, ngFB) {

	var uuid;
	
	function log(){
		console.log(arguments);
	}

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Register');
        }
        
		if ($window.localStorage.getItem("inc_session"))
		{
			//Redirect To Home App
			$state.go('dashboard');
		}
		
		//Reset Form
		$scope.register = {
		    email : '',
		    name : '',
		    password : '',
		    confirm : '',
		    state : '7'
		};
		
		API.getStates()
		.success(function(data) {
			$scope.states = data.data;
		})
		.error(function(data) {
			console.log('error states');
		});
	});
	
	//Open Login View
	$scope.login = function(event) {
		$state.go('login');
	}
	
	//Do Register View
	$scope.doRegister = function(register) {
		
		//Read Values
		var email = register.email;
		var name = register.name;
		var password = register.password;
		var idstate = register.state;
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		//Check Values
		if (name && email && password)
		{
			//Check Valid Email
            if (regex.test(email))
            {
	            //Send API Call
	            API.doRegister(email,name,password,idstate)
				.success(function(data) {
					if (data.status)
					{
						if (!data.data.status)
						{
							var alertPopup = $ionicPopup.alert({
								template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Tu registro ha sido exitoso. Te hemos mandado un correo electrónico, verifica tu dirección con el link incluído.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						
							alertPopup.then(function(res) {
								$state.go('login');
							});
						}
						else
						{
							$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
							$state.go('dashboard');
						}
					}
					else
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
				})
				.error(function(data) {
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de comunicación con el servidor.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
	        }
	        else
			{
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Escribe un email válido.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			}
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Llena todos los campos.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
	//goPrivacy
	$scope.privacy = function(event) {
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		var url = 'http://incorruptible.mx/privacidad/';
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'Privacidad'); 
			return false;
		}
	}
	
	//goTerms
	$scope.terms = function(event) {
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		var url = 'http://incorruptible.mx/condiciones/';
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'Privacidad'); 
			return false;
		}
	}
	
	//facebookLogin 
	$scope.facebookLogin = function(event) {
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			$cordovaOauth.facebook("204480250064720", ["email,public_profile"]).then(function(result) {
	        	var token = result.access_token;
	        	console.log(token);
	        	
				$ionicLoading.show({
			    	template: 'Procesando...',
					noBackdrop: false
			    });
			
				//Send API Call
	            API.loginFacebook(token)
				.success(function(data) {
					$ionicLoading.hide();
					if (data.status)
					{
						if (!data.data.status)
						{
							var alertPopup = $ionicPopup.alert({
								template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Tu registro ha sido exitoso. Te hemos mandado un correo electrónico, verifica tu dirección con el link incluído.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						
							alertPopup.then(function(res) {
								$state.go('login');
							});
						}
						else
						{
							$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
							$state.go('dashboard');
						}
					}
					else
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error al procesar la información (API Status).</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
				})
				.error(function(data) {
					$ionicLoading.hide();
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de conexión con el server (API 500).</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
						
	        }, function(error) {
		        console.log(error);
	        });
	   }
	   else
	   {
		   	ngFB.login({scope: 'email,public_profile'}).then(
		   	function (response) {
            	if (response.status === 'connected') {
                	console.log('Facebook login succeeded');
                	
                	var token = response.authResponse.accessToken;
		        	console.log(token);
		        	
					$ionicLoading.show({
				    	template: 'Procesando...',
						noBackdrop: false
				    });
				
					//Send API Call
		            API.loginFacebook(token)
					.success(function(data) {
						$ionicLoading.hide();
						if (data.status)
						{
							if (!data.data.status)
							{
								var alertPopup = $ionicPopup.alert({
									template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Tu registro ha sido exitoso. Te hemos mandado un correo electrónico, verifica tu dirección con el link incluído.</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
							
								alertPopup.then(function(res) {
									$state.go('login');
								});
							}
							else
							{
								$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
								$state.go('dashboard');
							}
						}
						else
						{
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error al procesar la información (API Status).</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
					})
					.error(function(data) {
						$ionicLoading.hide();
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de conexión con el server (API 500).</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
                	 
				} else {
                	var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Proceso de Inicio de Sesión cancelado.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				}
			});
	   }
	}
	
	//twitterLogin 
	$scope.twitterLogin = function(event) {
		var clientId = "gya0r31yP0565qMefn4QImAIx";
		var clientSecret = "7s4XvvTQkGhloVS9YB9Ux08R4z3SLhYojOFsHnGdYLAJ1ZtKGi";
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
			
		if (isIOS || isAndroid)
		{		
			$cordovaOauth.twitter(clientId,clientSecret, {redirect_uri: "http://10.0.2.2/callback"}).then(function(result){
				var token = result.oauth_token;
	        	var secret = result.oauth_token_secret;
	        	var id = result.user_id;
	        	var name = result.screen_name;
	        	
	        	$ionicLoading.show({
			      template: 'Procesando...',
			      duration: 3000
			    }).then(function(){
			       console.log("The loading indicator is now displayed");
			    });
			    
	        	API.getTwitterProfile(clientId,clientSecret,token,secret);
	        	$state.go('dashboard');
						
	        }, function(error) {
		        console.log(error);
	        });
	    }
	    else
	    {
		    hello.init({'twitter' : clientId});
		    
		    // Twitter instance
			var twitter = hello('twitter');
			var access_token = '';
			// Login
			twitter.login().then( function(r){
				
				access_token = r.authResponse.access_token;
				
				// Get Profile
				return twitter.api('account/verify_credentials.json?include_email=true');
				
			}, log )
			.then( function(p){
				
				$ionicLoading.show({
			    	template: 'Procesando...',
					noBackdrop: false
			    });
				
				//Send API Call
	            API.doLoginTwitter(p.email, p.screen_name, p.id_str, access_token, p.profile_image_url)
				.success(function(data) {
					if (data.status)
					{
						if (!data.data.status)
						{
							var alertPopup = $ionicPopup.alert({
								template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Tu registro ha sido exitoso. Te hemos mandado un correo electrónico, verifica tu dirección con el link incluído.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						
							alertPopup.then(function(res) {
								$state.go('login');
							});
						}
						else
						{
							$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
							$state.go('dashboard');
						}
						$ionicLoading.hide();
					}
					else
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
						$ionicLoading.hide();
					}
				})
				.error(function(data) {
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Error de comunicación con el servidor.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
					$ionicLoading.hide();
				});
			}, log );
	    }
	}
	
})

.controller('ForgotCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $ionicPopup) {

	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Forgot');
        }
        
		if ($window.localStorage.getItem("inc_session"))
		{
			//Redirect To Home App
			$state.go('dashboard');
		}
		
		$scope.forgot = {
		    email : ''
		};
		
		var height = $window.innerHeight;
		var margin = (height-200) / 2;
		
		$('.login .login-content .forgot').css('margin-top', margin);
	});
	
	//Forgot
	$scope.doForgot = function(forgot) {
		
		//Read Values
		var email = forgot.email;
		var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		//Check Values
		if (regex.test(email))
		{
			//Send API Call
            API.forgotPassword(email)
			.success(function(data) {
				if (data.status)
				{
					var alertPopup = $ionicPopup.alert({
						title: 'Don\'t eat that!',
						template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Revisa tu correo electrónico para continuar.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				
					alertPopup.then(function(res) {
						$state.go("welcome");
					});
				}
				else
				{
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">' + data.msg + '</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
					
					alertPopup.then(function(res) {
						$state.go("welcome");
					});
				}
			})
			.error(function(data) {
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">' + data.msg + '</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
				
				alertPopup.then(function(res) {
					$state.go("welcome");
				});
			});
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Escribe un correo electrónico válido.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
		
	}
	
	//Open Register View
	$scope.submit = function(event) {
		$state.go('welcome');
	}
	
	// Go Welcome
	$scope.goWelcome = function(event) {
		$state.go('welcome');
	}
})

.controller('ResetCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $ionicPopup) {

	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Reset');
        }
        
		if ($window.localStorage.getItem("inc_session"))
		{
			//Redirect To Home App
			$state.go('dashboard');
		}
		
		$scope.reset = {
		    password : '',
		    confirmPassword : '',
		    iduser : '',
		    token : ''
		};
		
		//Read Reset Info
		$scope.$apply(function() {
			
			if ($stateParams.userId) { $scope.iduser = $stateParams.userId; } 
			else { $scope.iduser = ''; }
			
			if ($stateParams.token) { $scope.token = $stateParams.token; }
			else { $scope.token = ''; }
		});
	});
	
	//Forgot
	$scope.doReset = function(reset) {
		
		//Read Values
		var iduser = $scope.iduser;
		var token = $scope.token;
		var password = reset.password;
		var confirm = reset.confirm;
		
		//Check Values
		if (iduser && token && password && confirm)
		{
			//Check Password
			if (password == confirm)
			{
				//Send API Call
		        API.resetPassword(iduser,token,password)
				.success(function(data) {
					if (data.status)
					{
						var alertPopup = $ionicPopup.alert({
							title: 'Don\'t eat that!',
							template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Tu contraseña se ha restablecido correctamente.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					
						alertPopup.then(function(res) {
							$state.go("login");
						});
					}
					else
					{
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">' + data.msg + '</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
				})
				.error(function(data) {
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">' + data.msg + '</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			}
			else
			{
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">La contraseña no coincide.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			}
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Llena todos los campos.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}	
})

.controller('DashboardCtrl', function($ionicPlatform, $scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $ionicPopup, $ionicModal) {

	var uuid;
	
	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Open Blog
  	$rootScope.goBlog = function(event) {
	  	$state.go('blog');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open News
  	$rootScope.goNews = function(event) {
	  	$state.go('news');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open My Complaints
  	$rootScope.goMyComplaints = function(event) {
	  	$state.go('MyComplaints');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open My Notifications
  	$rootScope.goMyNotifications = function(event) {
	  	$state.go('MyNotifications');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
	
	//Open Logout
  	$rootScope.goLogout = function(event) {
	  	$state.go('logout');
  	}
  	
  	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//Open Profile
  	$rootScope.goDashboard = function(event) {
	  	$state.go('dashboard');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open Profile
  	$rootScope.gotoProfile = function(event) {
	  	$state.go('Profile');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open Menu
	$rootScope.toggleLeft = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};
	
	//Open New Complaints
  	$rootScope.goNewComplaints = function(event) {
	  	$state.go('NewComplaints');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
	
	//Open Complaints Groups
  	$rootScope.goComplaintGroups = function(event) {
	  	$state.go('GroupsComplaints');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open New Complaint Group
  	$rootScope.goComplaintGroupNew = function(event) {
	  	//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #ong_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
        
        //Mostramos los botones de Acción
        $scope.showGroupActions = true;
	  	
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open Groups Dependency
  	$rootScope.goGroupsDependency = function(event) {
	  	//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
        
	  	$state.go('GroupsDependency');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open Complaints Dependency
  	$rootScope.goComplaintsDependency = function(event) {
	  	//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
        
	  	$state.go('ComplaintsDependency');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open Complaints Group Dependency
  	$rootScope.goComplaintGroupDependency = function(event) {
	  	//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
        
	  	$state.go('ComplaintsGroupDependency');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open Complaints Dependency
  	$rootScope.goComplaintsDependency = function(event) {
	  	//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
        
	  	$state.go('ComplaintsDependency');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	$('.dashboard .cover').on('click', function(e) {
	  	e.preventDefault();
	  	
	  	//Ocultamos el Lightbox y el InfoWindow
	  	$('.dashboard #map_info').css('display', 'none');
	  	$('.dashboard #ong_info').css('display', 'none');
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
	  	
	  	return false;
  	})
  	
  	//GeoLocator
  	$scope.geoLocator = function(event) {
	  	
	  	// Setup the loader
        $ionicLoading.show({
            //content: 'Loading',
            animation: 'fade-in',
            template: 'Procesando...',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });
         
        var onSuccess = function(position) {
            $ionicLoading.hide();
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            if (!lat) { lat = 19.4284700; }
            if (!lng) { lng = -99.1276600; }
            $scope.map.center = {
                latitude: lat,
                longitude: lng
            };
            $scope.options = {scrollwheel: false};
            $scope.coordsUpdates = 0;
            $scope.dynamicMoveCtr = 0;
            //$scope.randomMarkers = markers;
            $window.localStorage.setItem("lat", lat);
            $window.localStorage.setItem("lng", lng);
             
            var image_gps = {
              url: 'https://app.incorruptible.mx/img/pin3.svg',
              scaledSize: new google.maps.Size(36, 40)
            };
             
            $scope.marker = {
                id: 0,
                coords: angular.copy($scope.map.center),
                options: { 
                    draggable: true,
                    icon: image_gps,
                },
                events: {
                    dragend: function (marker, eventName, args) {
                        console.log('marker dragend');
                        var lat = marker.getPosition().lat();
                        var lon = marker.getPosition().lng();
                        console.log(lat);
                        console.log(lon);
                        $window.localStorage.setItem("lat", lat);
                        $window.localStorage.setItem("lng", lon);
                        $scope.map.center = {
                            latitude: lat,
                            longitude: lon
                        };
                         
                        var geocoder = new google.maps.Geocoder();
                        var latlng = new google.maps.LatLng(lat, lon);
                        var request = {
                            latLng: latlng
                        };
                         
                        geocoder.geocode(request, function(data, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (data[0] != null) {
                                    $scope.$apply(function() {
                                        $scope.location = data[0].formatted_address;
                                    });
                                } else {
                                    console.log("No hay dirección disponible");
                                }
                            }
                        });
         
                        $scope.marker.options = {
                            draggable: true,
                            //labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                            //labelAnchor: "100 0",
                            //labelClass: "marker-labels",
                            icon: image_gps
                        };
                    }
                }
            };
             
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            var request = {
                latLng: latlng
            };
             
            geocoder.geocode(request, function(data, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (data[0] != null) {
                        $scope.$apply(function() {
                            //$scope.location = data[0].formatted_address;
                        });
                    } else {
                        console.log("No hay dirección disponible");
                    }
                }
            });
             
            $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
                if (_.isEqual(newVal, oldVal))
                    return;
                $scope.coordsUpdates++;
            });
             
            /*
            //Send API Call
            API.getComplaints()
            .success(function(data) {
                if (data.status)
                {
                    //Read Complaints
                    angular.forEach(data.data, function(value, key) {
                        markers.push(createRandomMarker(value))
                    });
                }
                else
                {
                    console.log(data.msg);
                }
                 
            })
            .error(function(data) {
                console.log('error complaints');
            });
            */
            
            $scope.$apply();
             
            google.maps.event.trigger($scope.map,'resize');
        }
         
        function onError(error) {
            $ionicLoading.hide();
             
            console.log('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
             
            google.maps.event.trigger($scope.map,'resize');
        }
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
  	}
  	
  	var map;
  	var markers = [];
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Dashboard');
        }
        
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope.session);
			var image = 'img/user.png';
			$window.localStorage.setItem("lat", "19.3074852");
		    $window.localStorage.setItem("lng", "-99.1894499");
		    $scope.selectedMarkers = [];
		    $scope.showGroupActions = false;
		    $('#search-map input').val('');
				
			$scope.map = {
	            center: {
	              latitude: $rootScope.session.position.lat,
	              longitude: $rootScope.session.position.lng
	            },
	            zoom: 16,
	            options: {                    
	                streetViewControl: false,
	                mapTypeControl: false,
	                scaleControl: false,
	                rotateControl: false,
	                zoomControl: false,
	                disableDefaultUI: true
	            }
	        };
	        
	        $scope.options = {scrollwheel: false};
		    $scope.coordsUpdates = 0;
		    $scope.dynamicMoveCtr = 0;
		    $scope.randomMarkers = markers;
		    
		    $scope.filters = {
			    time: 'year',
			    category: 0,
			    amount: 0,
			    type: 1
		    };
		    
		    //Send API Call
		    switch ($rootScope.session.idprofile.toString())
		    {
			    case '1':API.getComplaints($rootScope.session.iduser,$scope.filters.time,$scope.filters.category,$scope.filters.amount,$scope.filters.type)
						.success(function(data) {
							if (data.status)
							{
								//Read Complaints
								angular.forEach(data.data.complaints, function(value, key) {
									markers.push(createRandomMarker(value))
								});
								
								//Root Filters
								$scope.map_categories = data.data.categories;
								$scope.map_amounts = data.data.amounts;
								$scope.map_types = data.data.types;
							}
							else
							{
								var alertPopup = $ionicPopup.alert({
								    title: 'Incorruptible',
								    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
							}
							
						})
						.error(function(data) {
							console.log('error complaints');
						});
						$('.button-filter').css('display', 'block');
						break;
				case '2':API.getComplaintsONG($rootScope.session.iduser,$scope.filters.time,$scope.filters.category,$scope.filters.amount,$scope.filters.type)
						.success(function(data) {
							if (data.status)
							{
								//Read Complaints
								angular.forEach(data.data.complaints, function(value, key) {
									markers.push(createRandomMarker(value))
								});
								
								//Root Filters
								$scope.map_categories = data.data.categories;
								$scope.map_amounts = data.data.amounts;
								$scope.map_types = data.data.types;
							}
							else
							{
								console.log(data.msg);
							}
							
						})
						.error(function(data) {
							console.log('error complaints');
						});
						$('.button-filter').css('display', 'block');
						break;
				case '3':API.getComplaintsDependency($rootScope.session.iduser,$scope.filters.time,$scope.filters.category,$scope.filters.amount,$scope.filters.type)
						.success(function(data) {
							if (data.status)
							{
								//Read Complaints
								angular.forEach(data.data.complaints, function(value, key) {
									markers.push(createRandomMarker(value))
								});
								
								//Root Filters
								$scope.map_categories = data.data.categories;
								$scope.map_amounts = data.data.amounts;
								$scope.map_types = data.data.types;
							}
							else
							{
								console.log(data.msg);
							}
							
						})
						.error(function(data) {
							console.log('error complaints');
						});
						$('.button-filter').css('display', 'none');
						break;
				default: break;
		    }
		    $scope.$apply();
		    
		    google.maps.event.trigger($scope.map,'resize');
		    
		    //Ocultamos el Lightbox y el InfoWindow
		    $('.dashboard #map_info').css('display', 'none');
		    $('.dashboard #ong_info').css('display', 'none');
	        $('.dashboard .cover').css('display', 'none');
	        
	        uiGmapGoogleMapApi.then(function(maps) {
		        // You can now merge your options which need google.map helpers
		        angular.merge($scope.map, {
			    	options: {
		            	mapTypeId: google.maps.MapTypeId.ROADMAP,
	                    zoomControlOptions: {
	                        style: google.maps.ZoomControlStyle.LARGE,
	                        position: google.maps.ControlPosition.LEFT_CENTER
	                    }
		            }
		        });
		        
		        var image_gps = {
		          url: 'https://app.incorruptible.mx/img/pin3.svg',
		          scaledSize: new google.maps.Size(36, 40)
		        };
				
			    $scope.marker = {
			    	id: 0,
					coords: {
			        	latitude: $rootScope.session.position.lat,
						longitude: $rootScope.session.position.lng
					},
					options: { 
						draggable: true,
						icon: image_gps,
					},
					events: {
						dragend: function (marker, eventName, args) {
							console.log('marker dragend');
							var lat = marker.getPosition().lat();
							var lon = marker.getPosition().lng();
							console.log(lat);
							console.log(lon);
							$window.localStorage.setItem("lat", lat);
						    $window.localStorage.setItem("lng", lon);
						    $scope.map.center = {
						        latitude: lat,
						        longitude: lon
						    };
						    
						    var geocoder = new google.maps.Geocoder();
							var latlng = new google.maps.LatLng(lat, lon);
							var request = {
					        	latLng: latlng
							};
							
							geocoder.geocode(request, function(data, status) {
					        	if (status == google.maps.GeocoderStatus.OK) {
									if (data[0] != null) {
										$scope.$apply(function() {
											$scope.location = data[0].formatted_address;
										});
									} else {
										console.log("No hay dirección disponible");
									}
								}
							});
			
							$scope.marker.options = {
								draggable: true,
								//labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
								//labelAnchor: "100 0",
								//labelClass: "marker-labels",
								icon: image_gps
							};
						}
					}
			    };
			    
			    maps.visualRefresh = true;
		    });
		
			$scope.$apply(function() {
				if ($rootScope.session.avatar) { image = $rootScope.session.avatar; }
				$rootScope.avatar = image;
				$rootScope.iduser = $rootScope.session.iduser;
				$rootScope.name = $rootScope.session.name;
				$rootScope.email = $rootScope.session.email;
				$rootScope.profile = $rootScope.session.profile;
				if ($rootScope.session.fbid) { $rootScope.fbid = true; }
				else { $rootScope.fbid = false; }
				if ($rootScope.session.twid) { $rootScope.twid = true; }
				else { $rootScope.twid = false; }
				if ($rootScope.session.idprofile == '1') { $rootScope.menuUser = true; }
				else { $rootScope.menuUser = false; }
				if ( $rootScope.idprofile = '2' ) { $rootScope.ong = true; }
				else { $rootScope.ong = false; }
				if ( $rootScope.idprofile = '3' ) { $rootScope.aut = true; }
				else { $rootScope.aut = false; }
				$rootScope.noComplaints = $rootScope.session.complaints;
				$rootScope.noFollows = $rootScope.session.follows;
			});
			
			var createRandomMarker = function(value) {
		    	var latitude = value.lat;
				var longitude = value.lng;
				var image_marker = {};
				
				switch (String(value.icon)) {
					case '1': 
				      	image_marker = {
					  		url: 'http://app.incorruptible.mx/img/red.png'
					  	};
				      	break
				    case '2': 
				      	image_marker = {
					  		url: 'http://app.incorruptible.mx/img/yellow.png'
					  	};
				      	break
				    case '3': 
				      	image_marker = {
					  		url: 'http://app.incorruptible.mx/img/green.png'
					  	};
				      	break
				    default: 
				      	image_marker = {
					  		url: 'http://app.incorruptible.mx/img/red.png'
					  	};
				      	break
				}
				
				var d = new Date(value.date.replace(' ', 'T'));
				
				var statusString = 'Status por definir';
				switch (value.status)
				{
					case '3': statusString = 'Denuncia enviada a la Autoridad'; break;
					case '6': statusString = 'Denuncia enviada a la Autoridad'; break;
					case '7': statusString = 'Denuncia enviada a la Autoridad'; break;
				}
				
				var ret = {
					id: value.idcomplaint,
		        	latitude: latitude,
					longitude: longitude,
					title: value.title,
					address: value.address,
					date: d,
					icon: image_marker,
					icon_value: value.icon,
					amount: value.amount,
					category: value.category,
					idgroup: value.idgroup,
					status: value.status,
					substatus: value.substatus,
					statusSTR: statusString
				};
				console.log(ret);
				return ret;
		    }; 
		    
		    $scope.placeMarker = function(){
	            console.log(this.getPlace());  
	            var loc = this.getPlace().geometry.location;
	            var lat = loc.lat();
	            var lng = loc.lng();
	            if (!lat) { lat = $rootScope.session.position.lat; }
		        if (!lng) { lng = $rootScope.session.position.lng; }
	            $scope.map.center = {
			        latitude: lat,
			        longitude: lng
			    };
			    $scope.options = {scrollwheel: false};
			    $scope.coordsUpdates = 0;
			    $scope.dynamicMoveCtr = 0;
			    $window.localStorage.setItem("lat", lat);
			    $window.localStorage.setItem("lng", lng);
			    
			    var geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng(lat, lng);
				var request = {
		        	latLng: latlng
				};
				
				geocoder.geocode(request, function(data, status) {
		        	if (status == google.maps.GeocoderStatus.OK) {
						if (data[0] != null) {
							$scope.$apply(function() {
								$scope.location = data[0].formatted_address;
							});
						} else {
							console.log("No hay dirección disponible");
						}
					}
				});
				
				var image_gps = {
		          url: 'https://app.incorruptible.mx/img/pin3.svg',
		          scaledSize: new google.maps.Size(36, 40)
		        };
				
			    $scope.marker = {
			    	id: 100,
					coords: {
			        	latitude: lat,
						longitude: lng
					},
					options: { 
						draggable: true,
						icon: image_gps,
					},
					events: {
						dragend: function (marker, eventName, args) {
							console.log('marker dragend');
							var lat = marker.getPosition().lat();
							var lon = marker.getPosition().lng();
							console.log(lat);
							console.log(lon);
							$window.localStorage.setItem("lat", lat);
						    $window.localStorage.setItem("lng", lon);
						    $scope.map.center = {
						        latitude: lat,
						        longitude: lon
						    };
						    
						    var geocoder = new google.maps.Geocoder();
							var latlng = new google.maps.LatLng(lat, lon);
							var request = {
					        	latLng: latlng
							};
							
							geocoder.geocode(request, function(data, status) {
					        	if (status == google.maps.GeocoderStatus.OK) {
									if (data[0] != null) {
										$scope.$apply(function() {
											$scope.location = data[0].formatted_address;
										});
									} else {
										console.log("No hay dirección disponible");
									}
								}
							});
			
							$scope.marker.options = {
								draggable: true,
								//labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
								//labelAnchor: "100 0",
								//labelClass: "marker-labels",
								icon: image_gps
							};
						}
					}
			    };
			    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
			    	if (_.isEqual(newVal, oldVal))
			    		return;
					$scope.coordsUpdates++;
			    });
			    
			    $scope.$apply();
			    
			    google.maps.event.trigger($scope.map,'resize');
	        };
		    
		    google.maps.event.trigger($scope.map,'resize'); 
			
			$scope.onClick = function(map, marker, eventName, model) {
		        
		        var lat = map.model.latitude;
		        var lng = map.model.longitude;
		        
		        var isIOS = ionic.Platform.isIOS();
				var isAndroid = ionic.Platform.isAndroid();
			
				$scope.map.center = {
			        latitude: lat,
			        longitude: lng
			    };
			    
			    $scope.$apply(function() {
				    $scope.map_info = eventName;
				    console.log(eventName);
				});
			 
			 	switch ($rootScope.session.idprofile)
			 	{
				 	case '1':
				 		//Mostramos el Lightbox y el InfoWindow
					    $('.dashboard #map_info').css('display', 'block');
					    $('.dashboard .cover').css('display', 'block');
					    break;
					case '2': 
					
						//Variable Temporal de Markers
						var check_pins = []
						
						//Eliminamos el Marker deseleccionado
						angular.forEach($scope.selectedMarkers, function(value, key) {
							//Verificamos
							if (value.key == map.key)
							{
								//Agregamos el Marker al arreglo temporal
								check_pins.push(value);
							}
						});
						
						if (check_pins.length)
						{
							//Asignamos el Icono Amarillo al Marker Seleccionado
							if (eventName.icon_value == 1)
							{
								map.setIcon("http://app.incorruptible.mx/img/red.png");
							}
							else
							{
								map.setIcon("http://app.incorruptible.mx/img/yellow.png");
							}
							
							//Variable Temporal de Markers
							var new_markers = []
							
							//Eliminamos el Marker deseleccionado
							angular.forEach($scope.selectedMarkers, function(value, key) {
								//Verificamos
								if (value.key != map.key)
								{
									//Agregamos el Marker al arreglo temporal
									new_markers.push(value);
								}
							});
							
							//Asignamos el nuevo arreglo de Markers
							$scope.selectedMarkers = new_markers;
							
							console.log($scope.selectedMarkers);
						}
						else
						{
							//Mostramos el Lightbox y el InfoWindow
							$('.dashboard #ong_info').css('display', 'block');
						    $('.dashboard .cover').css('display', 'block');
						    
						    $rootScope.pinSelected = map;
						}
						break;
					case '3':
				 		//Mostramos el Lightbox y el InfoWindow
					    $('.dashboard #aut_info').css('display', 'block');
					    $('.dashboard .cover').css('display', 'block');
					    break;
					default:
						break;
			 	}
	        }; 
	        
	        $scope.refreshMap();
	        
	        //selectTime
			$scope.selectTime = function(time)
			{
				//Update Time Filter
				$scope.filters.time = time;
				
				//Update Color Filter
				$('.modal-map-filters .button-bar a').removeClass('active');
				$('.modal-map-filters .button-bar #btn_' + time.toString()).addClass('active');
			}
			
			//setFilters
			$scope.setFilters = function(filters,map,modal)
			{
				//clear markers
				$scope.randomMarkers = [];
				markers = [];	
		        
		        //read getComplaints with filters
		        API.getComplaints($rootScope.session.iduser,filters.time,filters.category,filters.amount,filters.type)
				.success(function(data) {
					console.log(data);
					if (data.status)
					{
						//Read Complaints
						angular.forEach(data.data.complaints, function(value, key) {
							markers.push(createRandomMarker(value))
						});
						
						//Root Filters
						$scope.map_categories = data.data.categories;
						$scope.map_amounts = data.data.amounts;
						$scope.map_types = data.data.types;
						
						//Update Markers
						$scope.randomMarkers = markers;
						
						//Close Modal
						$scope.closeModal();
					}
					else
					{
						//Close Modal
						$scope.closeModal();
				
						//Show Alerts
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					console.log('error complaints');
				});
				
				//Clear Modal Open
				$('body').removeClass('modal-open');
			}
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
			
		if (isIOS)
		{
			var app = {
			    initialize: function() {
			        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
			    },
			    
			    onDeviceReady: function() {
			        this.registerForPush();
			    },
			    
			    registerForPush: function(){
			        var push = PushNotification.init({
			            ios: {
			                alert: "true",
			                badge: "true",
			                sound: "true"
			            }
			        });
			
			        push.on('registration', function(data) {
				        //registrationId
				        //registrationType
				        
				        API.registerDevice($rootScope.session.iduser,data.registrationId,'ios')
						.success(function(response) {
							console.log(response.msg);
						})
						.error(function(response) {
							console.log(response);
						});
				        
			            console.log('Registration Done: ' + JSON.stringify(data));
			        });
			
			        push.on('notification', function(data) {
				        //message
				        //additionalData.foreground
				        //additionalData.coldstart
				        
				        //Go to Notifications
				        
			            console.log('Notification Received' + JSON.stringify(data));            
			        });
			
			        push.on('error', function(e) {
			        	console.log('Notification Error', e);
			        });
			    }
			}
			
			app.initialize();
		}
		
		if (isAndroid)
		{
			Notification.register().then (function (success) {
			   // Success call back
			   console.log(success);
			}, function (error) {
			   // Error callback
			   console.log(error);
			})
		}
		
	});
	
	$scope.goViewComplaintsGroupDependency = function(idgroup)
	{
		//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
     
     	//Enviamos a la Vista de Comentario   
		$state.go('ViewComplaintGroup', { 'groupId': idgroup });
	}
	
	$scope.goViewComplaintDependency = function(idcomplaint)
	{
		//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #aut_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
     
     	//Enviamos a la Vista de Comentario   
		$state.go('ViewComplaint', { 'complaintId': idcomplaint });
	}
	
	$scope.refreshMap = function() {
        setTimeout(function () {
            $scope.refreshMap_();
        }, 1); //Need to execute it this way because the DOM may not be ready yet
    };

    $scope.refreshMap_ = function() {
      var div = document.getElementById("map_canvas");
        reattachMap($scope.map,div);
    };
    
    function reattachMap(map,div) {
	    if (!isDom(div)) {
	    	console.log("div is not dom");
			return map;
		} 
		else 
		{
			$('#map_canvas').append(div);
	    	
			while(div.parentNode) {
				div.style.backgroundColor = 'rgba(0,0,0,0)';
				div = div.parentNode;
			}
	
			return map;
		}
	}
	
	function isDom(element) {
	  return !!element &&
	         typeof element === "object" &&
	         "getBoundingClientRect" in element;
	}
	
	$scope.removeLightbox = function()
    {
        //Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #map_info').css('display', 'none');
        $('.dashboard #ong_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
    } 
	
	$rootScope.newComplaint = function(event) {
		
		console.log($scope.map);
		
		$state.go('newComplaint');
	}
	
	$scope.clearInput = function(event)
	{
		$('#search-map input').val('');
	}

	$scope.navTitle='<img class="title-image" src="img/logo.svg" />';
	
	$scope.groupComplaints = function(pin)
	{
		//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #ong_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
        
        //Mostramos los botones de Acción
        if (!$scope.showGroupsActions)
        {
        	$scope.showGroupActions = true;
        }
        
        //console.log($rootScope.pinSelected);
        
        //Verificamos el tipo de Marker
		if (pin.icon_value == 1 || pin.icon_value == 2)
		{
			var icono = '';
			if (angular.isObject(pin.icon))
			{
				//Asignamos el icono
				icono = pin.icon.url;
			}
			else
			{
				//Asignamos el icono
				icono = pin.icon;
			}
			
			//Verificamos si el Marker no ha sido seleccionado
			if (icono == "http://app.incorruptible.mx/img/red.png" || icono == "http://app.incorruptible.mx/img/yellow.png")
			{
				//Asignamos el Icono Amarillo al Marker Seleccionado
				//map.setIcon("http://app.incorruptible.mx/img/yellow.png");
				$rootScope.pinSelected.setIcon("http://app.incorruptible.mx/img/yellow_select.png");
			    
			    //Guardamos el Marker en el Arreglo
			    $scope.selectedMarkers.push($rootScope.pinSelected);
			}
			
			if (icono == "http://app.incorruptible.mx/img/yellow_select.png")
			{
				//Asignamos el Icono Amarillo al Marker Seleccionado
				if (pin.icon_value == 1)
				{
					$rootScope.pinSelected.setIcon("http://app.incorruptible.mx/img/red.png");
				}
				else
				{
					$rootScope.pinSelected.setIcon("http://app.incorruptible.mx/img/yellow.png");
				}
				
				//Variable Temporal de Markers
				var new_markers = []
				
				//Eliminamos el Marker deseleccionado
				angular.forEach($scope.selectedMarkers, function(value, key) {
					//Verificamos
					if (value.key != $rootScope.pinSelected.key)
					{
						//Agregamos el Marker al arreglo temporal
						new_markers.push(value);
					}
				});
				
				//Asignamos el nuevo arreglo de Markers
				$scope.selectedMarkers = new_markers;
			}
		}
		else
		{
			//Mostramos el Error
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Esta denuncia ya esta resuelta, no se puede agrupar.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
		
		console.log($scope.selectedMarkers);
	}
	
	$scope.viewComplaint = function(pin)
	{
		//View Complaint
		$state.go('ViewComplaint', { 'complaintId': pin.id });
	}
	
	$scope.createGroup = function(event)
	{
		//Verificamos si ha seleccionado alguna denuncia
		if ($scope.selectedMarkers.length)
		{
			//Creamos una confirmación
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de agrupar las denuncias seleccionadas?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			//Mostramos la confirmación
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Declaramos la Variable de Denuncias Seleccionadas
					var complaints = "";
					
					//Procesamos las denuncias seleccionadas
					angular.forEach($scope.selectedMarkers, function(value, key) {
						if (!complaints)
						{
							complaints = value.key;
						}
						else
						{
							complaints = complaints + ',' + value.key;
						}
					});
					
					//Guardamos en Session las Denuncias Seleccionadas
					$window.localStorage.setItem("selectedMarkers", complaints);

					//Mostramos la Pantalla para crear el grupo
					$state.go('newGroupComplaints');
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
		}
		else
		{
			//Mostramos el Error
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">No has seleccionado ninguna denuncia para agrupar.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
	$scope.cancelGroup = function(event)
	{
		//Ocultamos los Botones de Acción
		$scope.showGroupActions = false;
		
		//Reiniciamos el Arreglo de Markers Seleccionados
		$scope.selectedMarkers = [];
		
		//Reiniciamos los Markers al estado original
		var lat = $window.localStorage.getItem("lat");
        var lng = $window.localStorage.getItem("lng");
        if (!lat) { lat = $rootScope.session.position.lat; }
        if (!lng) { lng = $rootScope.session.position.lng; }
	    $scope.map.center = {
	        latitude: lat,
	        longitude: lng
	    };
	    $scope.options = {scrollwheel: false};
	    $scope.coordsUpdates = 0;
	    $scope.dynamicMoveCtr = 0;
	    $scope.randomMarkers = markers;
	    
	    //Send API Call
        API.getComplaints()
		.success(function(data) {
			if (data.status)
			{
				//Read Complaints
				angular.forEach(data.data, function(value, key) {
					markers.push(createRandomMarker(value))
				});
			}
			else
			{
				console.log(data.msg);
			}
			
		})
		.error(function(data) {
			console.log('error complaints');
		});
		
		var geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(lat, lng);
		var request = {
        	latLng: latlng
		};
		
		geocoder.geocode(request, function(data, status) {
        	if (status == google.maps.GeocoderStatus.OK) {
				if (data[0] != null) {
					$scope.$apply(function() {
						$scope.location = data[0].formatted_address;
					});
				} else {
					console.log("No hay dirección disponible");
				}
			}
		});
		
		var image_gps = {
          url: 'http://app.incorruptible.mx/img/pin3.svg',
          scaledSize: new google.maps.Size(36, 40)
        };
		
	    $scope.marker = {
	    	id: 0,
			coords: {
	        	latitude: lat,
				longitude: lng
			},
			options: { 
				draggable: true,
				icon: image_gps,
			},
			events: {
				dragend: function (marker, eventName, args) {
					console.log('marker dragend');
					var lat = marker.getPosition().lat();
					var lon = marker.getPosition().lng();
					console.log(lat);
					console.log(lon);
					$window.localStorage.setItem("lat", lat);
				    $window.localStorage.setItem("lng", lon);
				    $scope.map.center = {
				        latitude: lat,
				        longitude: lon
				    };
				    
				    var geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(lat, lon);
					var request = {
			        	latLng: latlng
					};
					
					geocoder.geocode(request, function(data, status) {
			        	if (status == google.maps.GeocoderStatus.OK) {
							if (data[0] != null) {
								$scope.$apply(function() {
									$scope.location = data[0].formatted_address;
								});
							} else {
								console.log("No hay dirección disponible");
							}
						}
					});
	
					$scope.marker.options = {
						draggable: true,
						//labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
						//labelAnchor: "100 0",
						//labelClass: "marker-labels",
						icon: image_gps
					};
				}
			}
	    };
	    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
	    	if (_.isEqual(newVal, oldVal))
	    		return;
			$scope.coordsUpdates++;
	    });
	}
	
	$scope.goViewComplaint = function(id) 
	{
		//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #map_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
		
		//Enviamos a la Vista de la Denuncia
		$state.go('ViewComplaint', { 'complaintId': id });
	}
	
	$scope.showJoinComplaint = function(id) 
	{	
		//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #map_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
     
     	//Enviamos a la Vista de Comentario   
		$state.go('AddCommentComplaint', { 'complaintId': id });
	}
	
	$scope.showGroupsComplaints = function(id)
	{
		$state.go('GroupsComplaints');
	}
	
	$scope.showGroupsComplaints = function(id)
	{
		$state.go('GroupsComplaints');
	}
	
	$scope.cancelGroupComplaint = function(id)
	{
		//Ocultamos el Lightbox y el InfoWindow
        $('.dashboard #ong_info').css('display', 'none');
        $('.dashboard .cover').css('display', 'none');
	}
	
	// A confirm dialog
	$scope.showConfirmFollow = function(id) {
		var iduser = $rootScope.session.iduser;
		var idcomplaint = id;
		
		var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">Ahora seguiras esta denuncía, te enviaremos una notificación cada vez que allá una actualización.</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				//Send API Call
		        API.followComplaint(idcomplaint,iduser)
				.success(function(data) {
					if (data.status)
					{
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">Denuncia seguida correctamente.</div>',
							cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
							cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
						
						//Ocultamos el Lightbox y el InfoWindow
				        $('.dashboard #map_info').css('display', 'none');
				        $('.dashboard .cover').css('display', 'none');
					}
					else
					{
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
	};
	
	$scope.disableTap = function(){
	    container = document.getElementsByClassName('pac-container');
	    // disable ionic data tab
	    angular.element(container).attr('data-tap-disabled', 'true');
	    // leave input field if google-address-entry is selected
	    angular.element(container).on("click", function(){
	        document.getElementById('location').blur();
	    });
	};
	
	//Modal Map Filters
	$ionicModal.fromTemplateUrl('templates/modal/map-filters.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});
	
	//showFilters click
	$scope.showFilters = function() {
		$scope.modal.show();
	}
	
	$scope.closeModal = function() {
		$scope.modal.hide();
	};
	
	// Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});
	
	// Execute action on hide modal
	$scope.$on('modal.hidden', function() {
		// Execute action
	});
	
	// Execute action on remove modal
	$scope.$on('modal.removed', function() {
		// Execute action
	});
})

.controller('IntroCtrl', function($ionicPlatform, $scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $ionicPopup, $ionicModal, $ionicSlideBoxDelegate) {

	var uuid;
	
	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Dashboard');
        }
        
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope.session);
		}
	});
	
	// Called to navigate to the main app
	$scope.startApp = function() {
	$state.go('dashboard');
	};
	$scope.next = function() {
	$ionicSlideBoxDelegate.next();
	};
	$scope.previous = function() {
	$ionicSlideBoxDelegate.previous();
	};
	
	// Called each time the slide changes
	$scope.slideChanged = function(index) {
	$scope.slideIndex = index;
	};
})

.controller('NewGroupComplaintsCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('New Group Complaints');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			$rootScope.selectedMarkers = $window.localStorage.getItem("selectedMarkers");
			
			//Reset Values
			$scope.group = {
			    titulo : '',
			    description : '',
			    level: '',
			    dependency: '',
			    term: ''
			};
			
			$scope.dependencies = {};
			$scope.dependenciesSelected = [];
			
			//Query Dependencies
			API.getDependencies()
			.success(function(data) {
				if (data.status)
				{
					//Leemos las Dependencias
					$scope.dependencies = data.data;
				}
				else
				{
					//Borramos los Resultados
					$scope.dependencies = {}
				}
				
			})
			.error(function(data) {
				console.log('error dependencies');
				$ionicLoading.hide();
			});
		}
		else
		{
			//Redirect Start
			$window.history.back();
		}
	});
	
	$( "#searchDependency" ).keyup(function(e) {
		e.preventDefault();
		
		var term = $(this).val();
		var number = term.length;
		
		if (number > 3)
		{
			//Send API Call
	        API.getDependenciesByTerm(term)
			.success(function(data) {
				if (data.status)
				{
					//Leemos las Dependencias
					$scope.dependencies = data.data;
				}
				else
				{
					//Borramos los Resultados
					$scope.dependencies = {}
				}
				
			})
			.error(function(data) {
				console.log('error dependencies');
				$ionicLoading.hide();
			});
		}
		else
		{
			//Borramos los Resultados
			$scope.dependencies = {}
		}
		
		return false;
	});
	
	//Search Dependencies
	$scope.getDependency = function(event,term)
	{
		//Send API Call
        API.getDependenciesByTerm(term)
		.success(function(data) {
			if (data.status)
			{
				//Leemos las Dependencias
				$scope.dependencies = data.data;
				
				//Ocultamos el Loading
				$ionicLoading.hide();
			}
			else
			{
				//Ocultamos el Loading
				$ionicLoading.hide();
			}
			
		})
		.error(function(data) {
			console.log('error dependencies');
			$ionicLoading.hide();
		});
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//saveGroupComplaint Click
  	$scope.saveGroupComplaint = function(group,dependenciesSelected) {
		
		//Declaramos la Variable de Denuncias Seleccionadas
		var title = group.titulo;
		var description = group.description;
		var level = group.level;
		var iduser = $rootScope.session.iduser;
		var complaints = $rootScope.selectedMarkers;
		var dependencyList = '';
		
		//Verificamos
		if (title && description && level && dependenciesSelected.length)
		{
			//Creamos una confirmación
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de crear el grupo de denuncias seleccionadas?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			//Mostramos la confirmación
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Read Dependency List
					angular.forEach(dependenciesSelected, function(value, key) {
						if (dependencyList)
						{
							dependencyList = dependencyList + ',' + value.iddependency;
						}
						else
						{
							dependencyList = value.iddependency;
						}
					});
					
					//Send API Call
			        API.saveGroupComplaints(title, description, level, dependencyList, iduser, complaints)
					.success(function(data) {
						console.log(data);
						if (data.status)
						{
							//Enviamos a la pagina de denuncias
							$state.go('GroupsComplaints');
						}
						else
						{
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">' + data.msg + '</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						console.log(data);
						console.log('error save group');
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
		
		}
		else
		{
			//Mostramos el Error
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Debes de llenar todos los campos y seleccionar el nivel y la dependencia.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
  	}
	
	//addDependency Click
	$scope.addDependency = function(dependency,group,dependenciesSelected)
	{
		var flag = false;
		
		//Read Category Name
		angular.forEach(dependenciesSelected, function(value, key) {
			if (value.iddependency == dependency.iddependency)
			{
				flag = true;
			}
		});
		
		if (!flag)
		{
			$scope.dependenciesSelected.push(dependency); 
			$scope.dependencies = {};
			$scope.group.term = '';
		}
		else
		{
			//Mostramos el Error
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Esta dependencia ya fue previamente seleccionada. Selecciona una dependencia diferente.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
	//removeDependency Click
	$scope.removeDependency = function(dependency,dependenciesSelected) {
		
		var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de eliminar la dependencia seleccionada?</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				$scope.dependenciesTemp = [];
				
				//Read Category Name
				angular.forEach(dependenciesSelected, function(value, key) {
					if (value.iddependency != dependency.iddependency)
					{
						$scope.dependenciesTemp.push(value);
					}
				});
				
				$scope.dependenciesSelected = $scope.dependenciesTemp;	
			}
		});
		
	}
})

.controller('GroupsComplaintsCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Groups Complaints');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			//Session
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			
			$scope.groups = {};
			
			//Procesamos el Comentario
			API.getGroupsByUser($rootScope.session.iduser)
			.success(function(data) {
				console.log(data);
				$scope.groups = data.data;
				
				$ionicLoading.hide();
			})
			.error(function(data) {
				$ionicLoading.hide();
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
	});
	
	//Open Dashboard
  	$scope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//
  	$scope.goViewGroup = function(event, idgroup)
  	{
	  	$state.go('ViewGroup', { 'groupId': idgroup });
  	}
	
})

.controller('GroupsDependencyCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Groups Dependency');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			//Session
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			
			$scope.groups = {};
			
			//Procesamos el Comentario
			API.getGroupsResolvedByDependency($rootScope.session.iduser)
			.success(function(data) {
				console.log(data);
				$scope.groups = data.data;
				
				$ionicLoading.hide();
			})
			.error(function(data) {
				$ionicLoading.hide();
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
	});
	
	//Open Dashboard
  	$scope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//
  	$scope.goViewGroup = function(event, idgroup)
  	{
	  	$state.go('ViewGroupDependency', { 'groupId': idgroup });
  	}
  	
  	$scope.goViewComplaintsGroupDependency = function(event, idgroup)
	{
		//Enviamos a la Vista de Comentario   
		$state.go('ViewComplaintGroup', { 'groupId': idgroup });
	}
	
})

.controller('ComplaintsGroupDependencyCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Groups Dependency');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			//Session
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			
			$scope.groups = {};
			console.log($rootScope.session.iduser);
			
			//Procesamos el Comentario
			API.getGroupsByDependency($rootScope.session.iduser)
			.success(function(data) {
				console.log(data);
				$scope.groups = data.data;
				
				$ionicLoading.hide();
			})
			.error(function(data) {
				console.log(data);
				$ionicLoading.hide();
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
	});
	
	//Open Dashboard
  	$scope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//
  	$scope.goViewGroup = function(event, idgroup)
  	{
	  	$state.go('ViewGroupDependency', { 'groupId': idgroup });
  	}
	
	$scope.goViewComplaintsGroupDependency = function(event, idgroup)
	{
		//Enviamos a la Vista de Comentario   
		$state.go('ViewComplaintGroup', { 'groupId': idgroup });
	}
})

.controller('ViewComplaintGroupCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Complaint Group Dependency');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			//Session
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			
			//Read IdComplaint
			var idgroup = $stateParams.groupId;
			console.log(idgroup);
			
			$scope.groups = {};
			
			//Check IdComplaint
			if (idgroup)
			{
				//Procesamos el Comentario
				API.getComplaintsGroupDependency(idgroup)
				.success(function(data) {
					console.log(data);
					$scope.complaints = data.data.complaints;
					$scope.group = data.data.group;
					
					$ionicLoading.hide();
				})
				.error(function(data) {
					$ionicLoading.hide();
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			}
			else
			{
				//Redirect Start
				$state.go('welcome');
			}
		}
	});
	
	//Open Dashboard
  	$scope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//
  	$scope.goViewComplaintDependency = function(event, idcomplaint)
  	{
	  	$state.go('ViewComplaintDependency', { 'complaintId': idcomplaint });
  	}
  	
  	$scope.goViewComplaint = function(event,id) 
	{
		//Enviamos a la Vista de la Denuncia
		$state.go('ViewComplaint', { 'complaintId': id });
	}
	
	//Date Format
	$scope.formatDate = function(date) {
		var d = new Date(date.replace(' ', 'T'));
		return new Date(d);
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Delete Complaint from Group
  	$scope.deleteComplaintGroup = function(complaint, group) {
	  	
	  	var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de eliminar esta denuncia del grupo de denuncias? Si el grupo se queda sin denuncias, quedará deshabilitado.</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				//Show Loading
			    $ionicLoading.show({
			    	template: 'Procesando...',
					noBackdrop: false
			    });
			    
				//Send API Call
		        API.deleteComplaintFromGroup(complaint.idcomplaint, group.idgroup, group.title, $rootScope.session.idprofile, $rootScope.session.iduser)
				.success(function(data) {
					console.log(data);
					if (data.status)
					{
						if (!data.data.complaints)
						{
							//Hide Loading
						    $ionicLoading.hide();
					    
							var alertPopup = $ionicPopup.alert({
								template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">No existen denuncias en este grupo, por lo que será deshabilitado.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
							
							alertPopup.then(function(res) {
						
								//Go to Dashboard
								$state.go('dashboard');
								
							});
						}
						else
						{
							//Hide Loading
						    $ionicLoading.hide();
						    
						    //Actualizamos la lista de Denuncias
						    $scope.complaints = data.data.complaints;
						    
						    if ($scope.complaints.length != 0)
						    {
							    var alertPopup = $ionicPopup.alert({
									template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Denuncia eliminada con éxito.</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
						    }
							else
							{
								var alertPopup = $ionicPopup.alert({
									template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">No existen denuncias en este grupo, por lo que será deshabilitado.</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
							
								$state.go('dashboard');
							}
						}
					}
					else
					{
						//Hide Loading
					    $ionicLoading.hide();
					    
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					
					//Hide Loading
					$ionicLoading.hide();
					    
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
  	}
})

.controller('NewComplaintsCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('New Complaints');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			//Session
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			
			$scope.complaints = {};
			
			//Procesamos el Comentario
			API.getNewComplaints()
			.success(function(data) {
				console.log(data);
				$scope.complaints = data.data;
				$ionicLoading.hide();
			})
			.error(function(data) {
				$ionicLoading.hide();
				console.log(data);
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
	});
	
	//Open Dashboard
  	$scope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//
  	$scope.goViewComplaint = function(event, idcomplaint)
  	{
	  	$state.go('ViewComplaint', { 'complaintId': idcomplaint });
  	}
	
})

.controller('ViewGroupCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicPopup) {
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('View Group');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			//console.log($rootScope.session);
			
			//Read idgroup
			var idgroup = $stateParams.groupId;
			
			//Check idgroup
			if (idgroup)
			{
				//Send API Call
		        API.getGroup(idgroup)
				.success(function(data) {
					//console.log(data);
					if (data.status)
					{
						$scope.group = data.data;
						console.log($scope.group);
					}
					else
					{
						console.log(data.msg);
					}
					
				})
				.error(function(data) {
					console.log('error view group');
				});
			}
			else
			{
				//Redirect Start
				$state.go('welcome');
			}
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Send Group
	$scope.sendGroup = function(group) {
		var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de enviar este grupo de denuncias a las autoridades?</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				//Send API Call
		        API.sendGroup(group.idgroup)
				.success(function(data) {
					if (data.status)
					{
						var alertPopup = $ionicPopup.alert({
							template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Grupo enviado con éxito.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
						
						alertPopup.then(function(res) {
					
							//Go to Dashboard
							$state.go('dashboard');
							
						});
					}
					else
					{
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
	}
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//Go to Complaint
  	$scope.goViewComplaint = function(complaint) {
    	$state.go('ViewComplaint', { 'complaintId': complaint.idcomplaint });
	}
  	
  	//Delete Complaint from Group
  	$scope.deleteComplaintGroup = function(complaint, group) {
	  	var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de eliminar esta denuncia del grupo de denuncias? Si el grupo se queda sin denuncias, quedará deshabilitado.</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				//Show Loading
			    $ionicLoading.show({
			    	template: 'Procesando...',
					noBackdrop: false
			    });
			    
				//Send API Call
		        API.deleteComplaintFromGroup(complaint.idcomplaint, group.idgroup, group.title, $rootScope.session.idprofile, $rootScope.session.iduser)
				.success(function(data) {
					if (data.status)
					{
						if (!data.data.complaints)
						{
							//Hide Loading
						    $ionicLoading.hide();
					    
							var alertPopup = $ionicPopup.alert({
								template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">No existen denuncias en este grupo, por lo que será deshabilitado.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
							
							alertPopup.then(function(res) {
						
								//Go to Dashboard
								$state.go('dashboard');
								
							});
						}
						else
						{
							//Hide Loading
						    $ionicLoading.hide();
						    
						    //Actualizamos la lista de Denuncias
						    $scope.group.complaints = data.data.complaints;
						    
						    if ($scope.group.complaints.length != 0)
						    {
							    var alertPopup = $ionicPopup.alert({
									template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Denuncia eliminada con éxito.</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
						    }
							else
							{
								var alertPopup = $ionicPopup.alert({
									template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">No existen denuncias en este grupo, por lo que será deshabilitado.</div></div>',
									okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
									okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
								});
							
								$state.go('dashboard');
							}
						}
					}
					else
					{
						//Hide Loading
					    $ionicLoading.hide();
					    
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					
					//Hide Loading
					$ionicLoading.hide();
					    
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
  	}
})

.controller('infoWindowCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicPopup) {
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Info Window');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope.session);
		}
	});
	
	$scope.goViewComplaint = function(event) {
    	var idcomplaint = $scope.$parent.model.id;
		$state.go('ViewComplaint', { 'complaintId': idcomplaint });
	}
	
	// A confirm dialog
	$scope.showConfirmFollow = function(event) {
		var iduser = $rootScope.session.iduser;
		var idcomplaint = $scope.$parent.model.id;
		
		var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">Ahora seguiras esta denuncía, te enviaremos una notificación cada vez que allá una actualización.</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				//Send API Call
		        API.followComplaint(idcomplaint,iduser)
				.success(function(data) {
					if (data.status)
					{
						$state.go('dashboard');
					}
					else
					{
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
	};
	
})

.controller('DashboardCtrlMenu', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture) {

	var uuid;
	
	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Open Blog
  	$rootScope.goBlog = function(event) {
	  	$state.go('blog');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open News
  	$rootScope.goNews = function(event) {
	  	$state.go('news');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
  	
  	//Open My Complaints
  	$rootScope.goMyComplaints = function(event) {
	  	$state.go('MyComplaints');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}
	
	//Open Logout
  	$rootScope.goLogout = function(event) {
	  	$state.go('logout');
  	}
  	
  	//Open Menu
	$rootScope.toggleLeft = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Dashboard Menu');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope.session);
			
			$ionicSideMenuDelegate.toggleLeft();
			
			$scope.$apply(function() {
				$rootScope.avatar = 'img/user.png';
				$rootScope.iduser = $rootScope.session.iduser;
				$rootScope.name = $rootScope.session.name;
				$rootScope.email = $rootScope.session.email;
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
		
		$scope.map = {
            center: {
              latitude: 19.3074852,
              longitude: -99.1894499
            },
            zoom: 15,
            options: {                    
                streetViewControl: false,
                mapTypeControl: false,
                scaleControl: false,
                rotateControl: false,
                zoomControl: false,
                disableDefaultUI: true
            }
        }; 
        
        var onSuccess = function(position) {
	        var lat = position.coords.latitude;
	        var lng = position.coords.longitude;
	        if (!lat) { lat = 19.3074852; }
			if (!lng) { lng = -99.1894499; }
	        
		    $scope.map.center = {
		        latitude: lat,
		        longitude: lng
		    };
		    $scope.options = {scrollwheel: false};
		    $scope.coordsUpdates = 0;
		    $scope.dynamicMoveCtr = 0;
		    $scope.marker = {
		    	id: 0,
				coords: {
		        	latitude: lat,
					longitude: lng
				},
				options: { draggable: true },
				events: {
		        	dragend: function (marker, eventName, args) {
						$log.log('marker dragend');
						var lat = marker.getPosition().lat();
						var lon = marker.getPosition().lng();
						$log.log(lat);
						$log.log(lon);
		
						$scope.marker.options = {
							draggable: true,
							labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
							labelAnchor: "100 0",
							labelClass: "marker-labels"
						};
					}
				}
		    };
		    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
		    	if (_.isEqual(newVal, oldVal))
		    		return;
				$scope.coordsUpdates++;
		    });
		    $scope.$apply();
		}
		
		function onError(error) {
		    console.log('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
		}
		navigator.geolocation.watchPosition(function(position) {
			navigator.geolocation.getCurrentPosition(onSuccess, onError);
		},
		function (error) { 
			if (error.code == error.PERMISSION_DENIED)
		    	console.log("you denied me :-(");
		   
			lat = 19.3074852;
			lng = -99.1894499;
	        
		    $scope.map.center = {
		        latitude: lat,
		        longitude: lng
		    };
		    $scope.options = {scrollwheel: false};
		    $scope.coordsUpdates = 0;
		    $scope.dynamicMoveCtr = 0;
		    $scope.marker = {
		    	id: 0,
				coords: {
		        	latitude: lat,
					longitude: lng
				},
				options: { draggable: true },
				events: {
		        	dragend: function (marker, eventName, args) {
						$log.log('marker dragend');
						var lat = marker.getPosition().lat();
						var lon = marker.getPosition().lng();
						$log.log(lat);
						$log.log(lon);
		
						$scope.marker.options = {
							draggable: true,
							labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
							labelAnchor: "100 0",
							labelClass: "marker-labels"
						};
					}
				}
		    };
		    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
		    	if (_.isEqual(newVal, oldVal))
		    		return;
				$scope.coordsUpdates++;
		    });
		    $scope.$apply();
		});     
		
		uiGmapGoogleMapApi.then(function(maps) {
	        // You can now merge your options which need google.map helpers
	        angular.merge($scope.map, {
	            options: {
	                mapTypeId: google.maps.MapTypeId.ROADMAP,
	                zoomControlOptions: {
	                    style: google.maps.ZoomControlStyle.LARGE,
	                    position: google.maps.ControlPosition.LEFT_CENTER
	                }
	            },
	        });
	        
	        
            
	    });
	});
	
	$rootScope.newComplaint = function(event) {
		console.log($scope.map);
		
		$state.go('newComplaint');
	}
	
	
})

.controller('NewComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaFile, $cordovaCapture, $cordovaFileTransfer, $ionicPopup, $ionicActionSheet, $cordovaCamera, $http) {

	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('New Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($window.localStorage);
			
			$rootScope.lat = $window.localStorage.getItem("lat");
			$rootScope.lng = $window.localStorage.getItem("lng");
			console.log($rootScope.lat);
			console.log($rootScope.lng);
			var titleComplaint = '';
			var descriptionComplaint = '';
			
			if ($rootScope.complaintData != undefined)
			{
				if ($rootScope.complaintData.title) { titleComplaint = $rootScope.complaintData.title; }
				if ($rootScope.complaintData.description) { descriptionComplaint = $rootScope.complaintData.description; }
			}
			else
			{
				//Read Video URL
				$rootScope.complaintData = {
					type : 'none',
					video : '',
					audio : '',
					image : '',
					title : titleComplaint,
					description: descriptionComplaint,
					lat: '',
					lng: '',
					address: '',
					category: '',
					amount: ''
				}
			}
			
			if ($rootScope.filesData == undefined)
			{
				$rootScope.filesData = [];
			}
			
			console.log($rootScope.complaintData);
			console.log($rootScope.filesData);
			
			$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
			$window.localStorage.setItem("filesData", JSON.stringify($rootScope.filesData));
			
			var isIOS = ionic.Platform.isIOS();
			var isAndroid = ionic.Platform.isAndroid();
			$scope.web = false;
			
			if (!isIOS || !isAndroid)
			{
				$scope.web = true;	
			}
				
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
		
	});
	
	//Video Select
	$scope.onVideoSelect = function (files) {
	    
	    //Show Loading
	    $ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
	    
	    //Read Values
	    var file = files[0];
	    console.log(file);
	    
	    if (file != undefined)
	    {
		    var filename = file.name;
		    var filesize = file.size;
		    var filetype = file.type;
		    var size = filesize/1000;
		    var type = filetype.split("/");
		    
		    //Check File Size < 10MB
		    if (size < 10240)
		    {
			    //Check File Type
			    if (type[0] == 'video')
			    {
				    API.uploadVideo(files[0])
					.success(function(data) {
						//Hide Loading
					    $ionicLoading.hide();
					    console.log(data);
						
						//Check Response
						if (data != 'error')
						{
							//Read Video URL
							$rootScope.complaintData.type = 'video';
							$rootScope.complaintData.video = data;
							
							//Send Video URL to Session
							$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
							
							//Go to Complaint Title
							$state.go('titleComplaint');
						}
						else
						{
							//Show Alert
						    var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
					})
					.error(function(error) {
						console.log(error);
						
						//Hide Loading
					    $ionicLoading.hide();
						
						//Show Alert
					    var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
			    }
			    else
			    {
				    //Hide Loading
				    $ionicLoading.hide();
				    
				    //Show Alert
				    var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El archivo seleccionado no es un video.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
			    }
		    }
		    else
		    {
			    //Hide Loading
			    $ionicLoading.hide();
			    
			    //Show Alert
			    var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El video debe pesar menos de 10MB.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
		    }
		}
		else
		{
			console.log('User cancel upload');
		}
	};
	
	$scope.onImageSelect = function (files) {
	    
	    //Show Loading
	    $ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
	    
	    //Read Values
	    var file = files[0];
	    console.log(file);
	    
	    if (file != undefined)
	    {
		    var filename = file.name;
		    var filesize = file.size;
		    var filetype = file.type;
		    var size = filesize/1000;
		    var type = filetype.split("/");
		    
		    //Check File Size < 2MB
		    if (size < 2048)
		    {
			    //Check File Type
			    if (type[0] == 'image')
			    {
				    API.uploadImage(files[0])
					.success(function(data) {
						//Hide Loading
					    $ionicLoading.hide();
						
						//Check Response
						if (data != 'error')
						{
							//Read Video URL
							$rootScope.complaintData.type = 'image';
							$rootScope.complaintData.video = data;
							
							//Send Video URL to Session
							$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
							
							//Go to Complaint Title
							$state.go('titleComplaint');
						}
						else
						{
							//Show Alert
						    var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
					})
					.error(function(error) {
						console.log(error);
						
						//Hide Loading
					    $ionicLoading.hide();
						
						//Show Alert
					    var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
			    }
			    else
			    {
				    //Hide Loading
				    $ionicLoading.hide();
				    
				    //Show Alert
				    var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El archivo seleccionado no es una imagen.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
			    }
		    }
		    else
		    {
			    //Hide Loading
			    $ionicLoading.hide();
			    
			    //Show Alert
			    var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El video debe pesar menos de 2MB.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
		    }
		}
		else
		{
			console.log('User cancel upload');
		}
	};
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Video Actions
	$scope.showVideoActions = function() {
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			if (isIOS)
			{
				$scope.takeVideo();
				/*
				$ionicActionSheet.show({
					buttons: [{text: 'Grabar Video'}],
					cancelText: 'Cancelar',
					cancel: function() {
						console.log('Cancel Video');
				    },
				    buttonClicked: function (index) {
		                if (index === 0) {
		                   $scope.loadVideo();
		                }
		                else if (index === 1) {
		                   $scope.takeVideo();
		                }
		 
		                //Returning true will close the actionSheet, false will keep it open
		                return true;
		            }
				});
				*/
			}
			else
			{
				$('#videoFile').trigger('click');
			}
			
		}
		else
		{
			//Load Video Input Dialog
			$('#videoFile').trigger('click');
		}
		
	}
	
	//Load Video from Device
	$scope.loadVideo = function() {
		
		//Load Video Input Dialog
		$('#videoFile').trigger('click');
		
	};
 	
 	//Capture Video
	$scope.takeVideo = function() {
		
		$ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
  
		// capture callback
		var captureSuccessVideo = function(mediaFiles) {
		    
	        path = mediaFiles[0].fullPath;
	        // do something interesting with the file
	        
	        // Destination URL
			var url = "https://api.incorruptible.mx/upload";
		 
			// File for Upload
			var targetPath = path;
			
			// File name only
			var filename = path.replace(/^.*[\\\/]/, '');
	        
	        var options = {
		    	fileKey: "file",
				fileName: filename,
				chunkedMode: false,
				mimeType: "multipart/form-data"
			};
			
			var params = new Object();
			params.type = 'video';
			params.filename = 'filename';
			params.filepath = path;
			
			options.params = params;
		 
			$cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
				$ionicLoading.hide();
				if (result.response != 'error')
				{
					//Read Video URL
					$rootScope.complaintData.type = 'video';
					$rootScope.complaintData.video = result.response;
					$rootScope.complaintData.image = '';
					
					//Send Video URL to Session
					$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
					
					//Go to Complaint Title
					$state.go('titleComplaint');
				}
				else
				{
					alert('Error');
				}
			}, function(err) {
				$ionicLoading.hide();
				console.log("ERROR: " + JSON.stringify(err));
				alert(JSON.stringify(err));
			}, function (progress) {
				// constant progress updates
				$timeout(function () {
					$scope.downloadProgress = (progress.loaded / progress.total) * 100;
				});
			});
		};
		
		// capture error callback
		var captureErrorVideo = function(error) {
		    //navigator.notification.alert('Error code: ' + error.code, null, 'No se capturo nada.');
		    $ionicLoading.hide();
		};
		
		// start video capture
		navigator.device.capture.captureVideo(captureSuccessVideo, captureErrorVideo, {limit:1,duration:30});
	}
	
	//Audio Actions
	$scope.showAudioActions = function() {
		$ionicActionSheet.show({
			buttons: [{text: 'Seleccionar Audio'}, {text: 'Grabar Audio'}],
			cancelText: 'Cancelar',
			cancel: function() {
				console.log('Cancel Audio');
		    },
		    buttonClicked: function (index) {
                if (index === 0) {
                   $scope.loadAudio();
                }
                else if (index === 1) {
                   $scope.takeAudio();
                }
 
                //Returning true will close the actionSheet, false will keep it open
                return true;
            }
		});
	}
	
	//Load Audio from Device
	$scope.loadAudio = function() {
		
		alert('funcionalidad pendiente');
		
	};
	
	//Capture Audio
	$scope.takeAudio = function() {
		
		$ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
  
		// capture callback
		var captureSuccessAudio = function(mediaFiles) {
		    var i, path, len;
		    for (i = 0, len = mediaFiles.length; i < len; i += 1)
		    {
		        path = mediaFiles[i].fullPath;
		        // do something interesting with the file
		        
		        // Destination URL
				var url = "https://api.incorruptible.mx/upload";
			 
				// File for Upload
				var targetPath = path;
				
				// File name only
				var filename = path.replace(/^.*[\\\/]/, '');
			 
				var options = {
			    	fileKey: "file",
					fileName: filename,
					chunkedMode: false,
					mimeType: "multipart/form-data"
				};
				
				var params = new Object();
				params.type = 'audio';
				params.filename = 'filename';
				params.filepath = path;
				
				options.params = params;
			 
				$cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
					$ionicLoading.hide();
					if (result.response != 'error')
					{
						//Read Video URL
						$rootScope.complaintData.type = 'audio';
						$rootScope.complaintData.audio = result.response;
						
						//Send Video URL to Session
						$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
						
						//Go to Complaint Title
						$state.go('titleComplaint');
					}
					else
					{
						alert('Error');
					}
				}, function(err) {
					$ionicLoading.hide();
					console.log("ERROR: " + JSON.stringify(err));
					alert(JSON.stringify(err));
				}, function (progress) {
					// constant progress updates
					$timeout(function () {
						$scope.downloadProgress = (progress.loaded / progress.total) * 100;
					});
				});
			}
		};
		
		// capture error callback
		var captureErrorAudio = function(error) {
		    //navigator.notification.alert('Error code: ' + error.code, null, 'No se capturo nada.');
		    $ionicLoading.hide();
		};
		
		// start audio capture
		navigator.device.capture.captureAudio(captureSuccessAudio, captureErrorAudio, {limit:1});
	}
	
	//Image Actions
	$scope.showImageActions = function() {
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			if (isIOS)
			{
				$scope.takeImage();
			}
			else
			{
				$('#imageFile').trigger('click');
			}
			
		}
		else
		{
			//Load Image Input Dialog
			$('#imageFile').trigger('click');
		}
	}
	
	//Load Image from Device
	$scope.loadImage = function() {
		
		//Load Image Input Dialog
		$('#imageFile').trigger('click');
		
	};
	
	//Capture Photo
	$scope.takeImage = function() {
		
		$ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
  
		// capture callback
		var captureSuccessImage = function(mediaFiles) {
		    var i, path, len;
		    for (i = 0, len = mediaFiles.length; i < len; i += 1)
		    {
		        path = mediaFiles[i].fullPath;
		        // do something interesting with the file
		        
		        // Destination URL
				var url = "https://api.incorruptible.mx/upload";
			 
				// File for Upload
				var targetPath = path;
				
				// File name only
				var filename = path.replace(/^.*[\\\/]/, '');
			 
				var options = {
			    	fileKey: "file",
					fileName: filename,
					chunkedMode: false,
					mimeType: "multipart/form-data"
				};
				
				var params = new Object();
				params.type = 'image';
				params.filename = 'filename';
				params.filepath = path;
				
				options.params = params;
			 
				$cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
					$ionicLoading.hide();
					if (result.response != 'error')
					{
						//Read Video URL
						$rootScope.complaintData.type = 'image';
						$rootScope.complaintData.image = result.response;
						$rootScope.complaintData.video = '';
						
						//Send Video URL to Session
						$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
						
						//Go to Complaint Title
						$state.go('titleComplaint');
					}
					else
					{
						alert('Error');
					}
				}, function(err) {
					$ionicLoading.hide();
					console.log("ERROR: " + JSON.stringify(err));
					alert(JSON.stringify(err));
				}, function (progress) {
					// constant progress updates
					$timeout(function () {
						$scope.downloadProgress = (progress.loaded / progress.total) * 100;
					});
				});
			}
		};
		
		// capture error callback
		var captureErrorImage = function(error) {
		    //navigator.notification.alert('Error code: ' + error.code, null, 'No se capturo nada.');
		    $ionicLoading.hide();
		};
		
		// start image capture
		navigator.device.capture.captureImage(captureSuccessImage, captureErrorImage, {limit:1});
	}
	
	$rootScope.goTitleComplaint = function(event) {
		$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
		$state.go('titleComplaint');
	}
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	// Add File
	$scope.addFile = function(complaint) {
		$('#fileComplaint').trigger('click');
	}
	
	$scope.onFileSelect = function (files,complaint) {
	    
	    //Show Loading
	    $ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
	    
	    //Read Values
	    var file = files[0];
	    var name = file.name;
	    var size = file.size;
	    var type = file.type.split("/");
	    var mb = parseFloat((size / (1024*1024)).toFixed(2));
	    var limit = 0;
	    if (type[0] == 'video') { limit = 10; } else { limit = 2; }
	    console.log(file);
	    
	    if (mb <= limit)
	    {
		    if (file != undefined)
		    {
			    var filename = file.name;
			    var filesize = file.size;
			    var filetype = file.type;
			    var size = filesize/1000;
			    var type = filetype.split("/");
			    
			    API.uploadFileNew(files[0],type[0],filename,mb,filetype)
				.success(function(data) {
					//Hide Loading
				    $ionicLoading.hide();
					
					//Check Response
					if (data.status)
					{
						//Read Video URL
						$rootScope.filesData.push(data.data);
						console.log(data);
					}
					else
					{
						//Show Alert
					    var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
				})
				.error(function(error) {
					console.log(error);
					
					//Hide Loading
				    $ionicLoading.hide();
					
					//Show Alert
				    var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			}
			else
			{
				//Hide Loading
			    $ionicLoading.hide();
			    
				console.log('User cancel upload');
			}
		}
		else
		{
			//Hide Loading
		    $ionicLoading.hide();
		    
		    //Show Alert
		    var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El archivo debe pesar menos de ' + limit + 'MB.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	};
	
	$scope.showFile = function(url) {
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'file'); 
			return false;
		}
	}
	
})

.controller('TitleComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Title Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			$rootScope.complaintData = JSON.parse($window.localStorage.getItem("complaintData"));
			
			$rootScope.lat = $window.localStorage.getItem("lat");
			$rootScope.lng = $window.localStorage.getItem("lng");
			if (!$rootScope.lat) { $rootScope.lat = 19.3074852; }
			if (!$rootScope.lng) { $rootScope.lng = -99.1894499; }
			
			//Reset Form
			$scope.complaint = {
			    title : $rootScope.complaintData.title,
			    description : $rootScope.complaintData.description,
			    search : '',
			    lat: '',
			    lng: '',
			    address: ''
			};
			
			console.log($rootScope.complaintData);
			
			$scope.map = {
	            center: {
	              latitude: $rootScope.lat,
	              longitude: $rootScope.lng
	            },
	            zoom: 15,
	            options: {                    
	                streetViewControl: false,
	                mapTypeControl: false,
	                scaleControl: false,
	                rotateControl: false,
	                zoomControl: false,
	                disableDefaultUI: true
	            }
	        }; 
	        
			uiGmapGoogleMapApi.then(function(maps) {
		        // You can now merge your options which need google.map helpers
		        angular.merge($scope.map, {
		            options: {
		                mapTypeId: google.maps.MapTypeId.ROADMAP,
		                zoomControlOptions: {
		                    style: google.maps.ZoomControlStyle.LARGE,
		                    position: google.maps.ControlPosition.LEFT_CENTER
		                }
		            }
		        });
		        
		        var geocoder = new google.maps.Geocoder();
				var latlng = new google.maps.LatLng($rootScope.lat, $rootScope.lng);
				
				var request = {
		        	latLng: latlng
				};
				
				$scope.address = 'Buscando...';
				
				geocoder.geocode(request, function(data, status) {
		        	if (status == google.maps.GeocoderStatus.OK) {
						if (data[0] != null) {
							$scope.$apply(function() {
								$scope.address = data[0].formatted_address;
								$rootScope.complaintData.address = data[0].formatted_address;
								$rootScope.complaintData.lat = $rootScope.lat;
							    $rootScope.complaintData.lng = $rootScope.lng;
							});
						} else {
							console.log("No hay dirección disponible");
						}
					}
				});
		        
		        var image_gps = {
		          url: 'http://app.incorruptible.mx/img/pin3.svg',
		          scaledSize: new google.maps.Size(36, 40)
		        };
		        
		        $scope.marker = {
			    	id: 0,
					coords: {
			        	latitude: $rootScope.lat,
						longitude: $rootScope.lng
					},
					options: { 
						draggable: true,
						icon: image_gps,
					},
					events: {
			        	dragend: function (marker, eventName, args) {
							console.log('marker dragend');
							var lat = marker.getPosition().lat();
							var lon = marker.getPosition().lng();
							var latlng = new google.maps.LatLng(lat, lon);
							var request = {
					        	latLng: latlng
							};
							$rootScope.complaintData.lat = lat;
						    $rootScope.complaintData.lng = lon;
						    $window.localStorage.setItem("lat", lat);
						    $window.localStorage.setItem("lng", lon);
							
							geocoder.geocode(request, function(data, status) {
					        	if (status == google.maps.GeocoderStatus.OK) {
									if (data[0] != null) {
										$scope.$apply(function() {
											$scope.map = {
									            center: {
									              latitude: lat,
									              longitude: lon
									            }
									        };
											$scope.address = data[0].formatted_address;
											$rootScope.complaintData.address = data[0].formatted_address;
											$rootScope.complaintData.lat = $rootScope.lat;
										    $rootScope.complaintData.lng = $rootScope.lng;
										});
									} else {
										console.log("No hay dirección disponible");
									}
								}
							});
						}
					}
			    };   
		        
		    });
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event, complaint) {
		
		//Read Values
		if (complaint != undefined)
		{
			var title = complaint.title;
			var description = complaint.description;
		}
		else
		{
			var title = '';
			var description = '';
		}
		$rootScope.lat = $window.localStorage.getItem("lat");
		$rootScope.lng = $window.localStorage.getItem("lng");
		
		//Check Values
		if (title && description)
		{
			//Assing Values
			$rootScope.complaintData.title = title;
			$rootScope.complaintData.description = description;
			$rootScope.complaintData.lat = $rootScope.lat;
		    $rootScope.complaintData.lng = $rootScope.lng;
		}
		    
		$window.history.back();
	};
	
	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	}
	
	//Go to Category Complaint
	$rootScope.goCategoryComplaint = function(complaint) {
		
		//Read Values
		var title = complaint.title;
		var description = complaint.description;
		//$rootScope.lat = $window.localStorage.getItem("lat");
		//$rootScope.lng = $window.localStorage.getItem("lng");
		
		//Check Values
		if (title && description)
		{
			//Assing Values
			$rootScope.complaintData.title = title;
			$rootScope.complaintData.description = description;
			$rootScope.complaintData.lat = $rootScope.lat;
		    $rootScope.complaintData.lng = $rootScope.lng;
			
			//Save Values
			$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
			
			//Go to Category Complaint
			$state.go('CategoryComplaint');
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Escribe un título y comentario para la denuncia.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}

	$scope.placeMarker = function(){
        console.log(this.getPlace());  
        var loc = this.getPlace().geometry.location;
        var lat = loc.lat();
        var lng = loc.lng();
        if (!lat) { lat = 19.3074852; }
        if (!lng) { lng = -99.1894499; }
        $scope.map.center = {
	        latitude: lat,
	        longitude: lng
	    };
	    $scope.options = {scrollwheel: false};
	    $scope.coordsUpdates = 0;
	    $scope.dynamicMoveCtr = 0;
	    $window.localStorage.setItem("lat", lat);
	    $window.localStorage.setItem("lng", lng);
	    
	    var geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(lat, lng);
		var request = {
        	latLng: latlng
		};
		
		geocoder.geocode(request, function(data, status) {
        	if (status == google.maps.GeocoderStatus.OK) {
				if (data[0] != null) {
					$scope.$apply(function() {
						$scope.map = {
				            center: {
				              latitude: lat,
				              longitude: lng
				            }
				        };
						$scope.address = data[0].formatted_address;
						$rootScope.complaintData.address = data[0].formatted_address;
						$rootScope.complaintData.lat = $rootScope.lat;
					    $rootScope.complaintData.lng = $rootScope.lng;
					    $('#searchPlace').val('');
					});
				} else {
					console.log("No hay dirección disponible");
				}
			}
		});
		
		var image_gps = {
          url: 'https://app.incorruptible.mx/img/pin3.svg',
          scaledSize: new google.maps.Size(36, 40)
        };
		
	    $scope.marker = {
	    	id: 100,
			coords: {
	        	latitude: lat,
				longitude: lng
			},
			options: { 
				draggable: true,
				icon: image_gps,
			},
			events: {
				dragend: function (marker, eventName, args) {
					console.log('marker dragend');
					var lat = marker.getPosition().lat();
					var lon = marker.getPosition().lng();
					console.log(lat);
					console.log(lon);
					$window.localStorage.setItem("lat", lat);
				    $window.localStorage.setItem("lng", lon);
				    $scope.map.center = {
				        latitude: lat,
				        longitude: lon
				    };
				    
				    var geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(lat, lon);
					var request = {
			        	latLng: latlng
					};
					
					geocoder.geocode(request, function(data, status) {
			        	if (status == google.maps.GeocoderStatus.OK) {
							if (data[0] != null) {
								$scope.$apply(function() {
									$scope.address = data[0].formatted_address;
									$('#searchPlace').val('');
								});
							} else {
								console.log("No hay dirección disponible");
							}
						}
					});
	
					$scope.marker.options = {
						draggable: true,
						//labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
						//labelAnchor: "100 0",
						//labelClass: "marker-labels",
						icon: image_gps
					};
				}
			}
	    };
	    $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
	    	if (_.isEqual(newVal, oldVal))
	    		return;
			$scope.coordsUpdates++;
	    });
	    
	    $scope.$apply();
	    
	    google.maps.event.trigger($scope.map,'resize');
    };
    
    $scope.clearInput = function(event)
	{
		$('#searchPlace').val('');
	}
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	$scope.disableTap = function(){
	    container = document.getElementsByClassName('pac-container');
	    // disable ionic data tab
	    angular.element(container).attr('data-tap-disabled', 'true');
	    // leave input field if google-address-entry is selected
	    angular.element(container).on("click", function(){
	        document.getElementById('searchPlace').blur();
	    });
	};
})

.controller('CategoryComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $cordovaMedia, $sce) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Category Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$ionicLoading.show({
		      template: 'Procesando...',
		      duration: 3000
		    }).then(function(){
		       console.log("The loading indicator is now displayed");
		    });
	    
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));	
			$rootScope.complaintData = JSON.parse($window.localStorage.getItem("complaintData"));
			
			//Reset Form
			$scope.complaint = {
			    title : '',
			    description : '',
			    search : '',
			    lat: '',
			    lng: '',
			    address: '',
			    follow: 0,
			    type: '',
			    video: '',
			    audio: '',
			    image: ''
			};
			
			$scope.isIOS = ionic.Platform.isIOS();
			$scope.isAndroid = ionic.Platform.isAndroid();
			
			//Send API Call
            API.getCategories()
			.success(function(data) {
				if (data.status)
				{
					$scope.complaint.categories = data.data;
				}
				else
				{
					var alertPopup = $ionicPopup.alert({
					   	title: 'Incorruptible',
					    template: data.msg
					});
				}
			})
			.error(function(data) {
				var alertPopup = $ionicPopup.alert({
				     title: 'Incorruptible',
				     template: data.msg
				});
			});
			
			$scope.complaint.title = $rootScope.complaintData.title;
			$scope.complaint.description = $rootScope.complaintData.description;
			$scope.complaint.address = $rootScope.complaintData.address;
			$scope.complaint.lat = $rootScope.complaintData.lat;
			$scope.complaint.lng = $rootScope.complaintData.lng;
			if ($rootScope.complaintData.type == undefined)
			{
				$rootScope.complaintData.type = 'none';
			}
			$scope.complaint.type = $rootScope.complaintData.type;
			$scope.complaint.video = $rootScope.complaintData.video;
			$scope.complaint.audio = $rootScope.complaintData.audio;
			$scope.complaint.image = $rootScope.complaintData.image;
					
			console.log($scope.complaint);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Go to Amount Complaint
	$rootScope.goAmountComplaint = function(event,idcategory) {
		//Assign ID Category
		$rootScope.complaintData.category = idcategory;
		
		//Read Category Name
		angular.forEach($scope.complaint.categories, function(value, key) {
			if (value.idcategory === idcategory)
			{
				$rootScope.complaintData.categoryName = value.name;
			}
		});
		
		//Save Values
		$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
		
		//Go to Amount Complaint
		$state.go('AmountComplaint');
	}
	
	//Show Media
	$scope.showMedia = function(event) {
		
		if ($scope.complaint.type == 'video')
		{
			//Play Video
			if ($('.detail .video-player video').get(0).paused)
			{
		  		$('.detail .video-player video')[0].load();
		  		$('.detail .video-player video').get(0).play();
		  		$('.detail .video-player video').prop("controls",true);
		  		$('.detail .video-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').get(0).pause();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .video-player video').on('ended',function(){
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		    });
		}
		
		if ($scope.complaint.type == 'audio')
		{
			//Play Video
			if ($('.detail .audio-player audio').get(0).paused)
			{
		  		$(".detail .audio-player audio")[0].load();
		  		$('.detail .audio-player audio').get(0).play();
		  		$('.detail .audio-player audio').prop("controls",true);
		  		$('.detail .audio-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$(".detail .audio-player audio")[0].load();
			  	$('.detail .audio-player audio').get(0).pause();
			  	$('.detail .audio-player audio').prop("controls",false);
			  	$('.detail .audio-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .audio-player audio').on('ended',function(){
			  	$('.detail .audio-player audio')[0].load();
			  	$('.detail .audio-player audio').prop("controls",false); 
			  	$('.detail .audio-player .play').fadeIn('slow');
		    });	
		}
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
})

.controller('AmountComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $sce) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Amount Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$ionicLoading.show({
		      template: 'Procesando...',
		      duration: 3000
		    }).then(function(){
		       console.log("The loading indicator is now displayed");
		    });
		    
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));	
			$rootScope.complaintData = JSON.parse($window.localStorage.getItem("complaintData"));
			
			//Reset Form
			$scope.complaint = {
			    title : '',
			    description : '',
			    search : '',
			    lat: '',
			    lng: '',
			    address: '',
			    follow: 0,
			    type: '',
			    video: '',
			    audio: '',
			    image: ''
			};
			
			$scope.isIOS = ionic.Platform.isIOS();
			$scope.isAndroid = ionic.Platform.isAndroid();
			
			//Send API Call
            API.getAmounts()
			.success(function(data) {
				if (data.status)
				{
					$scope.complaint.amounts = data.data;
				}
				else
				{
					var alertPopup = $ionicPopup.alert({
					   	title: 'Incorruptible',
					    template: data.msg
					});
				}
			})
			.error(function(data) {
				var alertPopup = $ionicPopup.alert({
				     title: 'Incorruptible',
				     template: data.msg
				});
			});
			
			$scope.$apply(function() {
				$scope.complaint.title = $rootScope.complaintData.title;
				$scope.complaint.address = $rootScope.complaintData.address;
				$scope.complaint.type = $rootScope.complaintData.type;
				$scope.complaint.video = $rootScope.complaintData.video;
				$scope.complaint.audio = $rootScope.complaintData.audio;
				$scope.complaint.image = $rootScope.complaintData.image;
				$scope.complaint.categoryName = $rootScope.complaintData.categoryName;
			});
					
			console.log($rootScope.complaintData);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Go to Verify Complaint
	$rootScope.goVerifyComplaint = function(event,idamount) {
		
		//Assign ID Amount
		$rootScope.complaintData.amount = idamount;
		
		//Read Category Name
		angular.forEach($scope.complaint.amounts, function(value, key) {
			if (value.idamount === idamount)
			{
				$rootScope.complaintData.amountValue = value.value;
			}
		});
		
		//Save Values
		$window.localStorage.setItem("complaintData", JSON.stringify($rootScope.complaintData));
		
		$state.go('VerifyComplaint');
	}
	
	//Show Media
	$scope.showMedia = function(event) {
		
		if ($scope.complaint.type == 'video')
		{
			//Play Video
			if ($('.detail .video-player video').get(0).paused)
			{
		  		$('.detail .video-player video')[0].load();
		  		$('.detail .video-player video').get(0).play();
		  		$('.detail .video-player video').prop("controls",true);
		  		$('.detail .video-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').get(0).pause();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .video-player video').on('ended',function(){
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		    });
		}
		
		if ($scope.complaint.type == 'audio')
		{
			//Play Video
			if ($('.detail .audio-player audio').get(0).paused)
			{
		  		$(".detail .audio-player audio")[0].load();
		  		$('.detail .audio-player audio').get(0).play();
		  		$('.detail .audio-player audio').prop("controls",true);
		  		$('.detail .audio-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$(".detail .audio-player audio")[0].load();
			  	$('.detail .audio-player audio').get(0).pause();
			  	$('.detail .audio-player audio').prop("controls",false);
			  	$('.detail .audio-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .audio-player audio').on('ended',function(){
			  	$('.detail .audio-player audio')[0].load();
			  	$('.detail .audio-player audio').prop("controls",false); 
			  	$('.detail .audio-player .play').fadeIn('slow');
		    });	
		}
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Back to Category
	$scope.backToCategory = function(event) {
		$state.go('CategoryComplaint');
	}
 })

.controller('VerifyComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Verify Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$ionicLoading.show({
		      template: 'Procesando...',
		      duration: 3000
		    }).then(function(){
		       console.log("The loading indicator is now displayed");
		    });
		    
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));	
			$rootScope.complaintData = JSON.parse($window.localStorage.getItem("complaintData"));
			
			//Reset Form
			$scope.complaint = {
			    title : '',
			    description : '',
			    search : '',
			    lat: '',
			    lng: '',
			    address: '',
			    follow: 0,
			    type: '',
			    video: '',
			    audio: '',
			    image: ''
			};
			
			$scope.isIOS = ionic.Platform.isIOS();
			$scope.isAndroid = ionic.Platform.isAndroid();
			
			$scope.complaint.title = $rootScope.complaintData.title;
			$scope.complaint.description = $rootScope.complaintData.description;
			$scope.complaint.address = $rootScope.complaintData.address;
			$scope.complaint.type = $rootScope.complaintData.type;
			$scope.complaint.video = $rootScope.complaintData.video;
			$scope.complaint.audio = $rootScope.complaintData.audio;
			$scope.complaint.image = $rootScope.complaintData.image;
			$scope.complaint.categoryName = $rootScope.complaintData.categoryName;
			$scope.complaint.amountValue = $rootScope.complaintData.amountValue;
					
			console.log($rootScope.complaintData);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	// A confirm dialog
	$scope.sendComplaint = function() {
		
		//Send API Call
        API.saveComplaint($rootScope.complaintData,$rootScope.session.iduser)
		.success(function(data) {
			if (data.status)
			{
				//Show Alert
				var alertPopup = $ionicPopup.alert({
					template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15 centered"><div class="space20">¡Gracias por hacer una denuncia!<br/><br />Será revisada por nuestro equipo antes de aparecer a los demás usuarios.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			
				alertPopup.then(function(res) {
					
					//Clear Data
					$rootScope.complaintData = {};
					$scope.complaint = {}
					
					//Go to Dashboard
					$state.go('dashboard');
					
				});
			}
			else
			{
				//Show Alert
				var alertPopup = $ionicPopup.alert({
				   	title: 'Incorruptible',
				    template: data.msg
				});
			}
		})
		.error(function(data) {
			//Show Alert
			var alertPopup = $ionicPopup.alert({
			     title: 'Incorruptible',
			     template: data.msg
			});
		});
	};
	
	//Show Media
	$scope.showMedia = function(event) {
		
		if ($scope.complaint.type == 'video')
		{
			//Play Video
			if ($('.detail .video-player video').get(0).paused)
			{
		  		$('.detail .video-player video')[0].load();
		  		$('.detail .video-player video').get(0).play();
		  		$('.detail .video-player video').prop("controls",true);
		  		$('.detail .video-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').get(0).pause();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .video-player video').on('ended',function(){
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		    });
		}
		
		if ($scope.complaint.type == 'audio')
		{
			//Play Video
			if ($('.detail .audio-player audio').get(0).paused)
			{
		  		$(".detail .audio-player audio")[0].load();
		  		$('.detail .audio-player audio').get(0).play();
		  		$('.detail .audio-player audio').prop("controls",true);
		  		$('.detail .audio-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$(".detail .audio-player audio")[0].load();
			  	$('.detail .audio-player audio').get(0).pause();
			  	$('.detail .audio-player audio').prop("controls",false);
			  	$('.detail .audio-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .audio-player audio').on('ended',function(){
			  	$('.detail .audio-player audio')[0].load();
			  	$('.detail .audio-player audio').prop("controls",false); 
			  	$('.detail .audio-player .play').fadeIn('slow');
		    });	
		}
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Back to Category
	$scope.backToCategory = function(event) {
		$state.go('CategoryComplaint');
	}
	
	//Back to Amount
	$scope.backToAmount = function(event) {
		$state.go('AmountComplaint');
	}
	
	//Back to Title
	$scope.backToTitle = function(event) {
		$state.go('titleComplaint');
	}
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
})

.controller('VerifyComplaintSendCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Verify Complaint Send');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$ionicLoading.show({
		      template: 'Procesando...',
		      duration: 3000
		    }).then(function(){
		       console.log("The loading indicator is now displayed");
		    });
		    
		    $scope.isIOS = ionic.Platform.isIOS();
			$scope.isAndroid = ionic.Platform.isAndroid();
		    
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));	
			$scope.sendComplaint();		
			console.log($rootScope);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	// A confirm dialog
	$scope.sendComplaint = function() {
		
		var alertPopup = $ionicPopup.alert({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Denuncia publicada.</div></div>',
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});
	
		alertPopup.then(function(res) {
			$state.go('dashboard');
		});
		
	};
	
	//Show Media
	$scope.showMedia = function(event) {
		
		if ($scope.complaint.type == 'video')
		{
			//Play Video
			if ($('.detail .video-player video').get(0).paused)
			{
		  		$('.detail .video-player video')[0].load();
		  		$('.detail .video-player video').get(0).play();
		  		$('.detail .video-player video').prop("controls",true);
		  		$('.detail .video-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').get(0).pause();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .video-player video').on('ended',function(){
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		    });
		}
		
		if ($scope.complaint.type == 'audio')
		{
			//Play Video
			if ($('.detail .audio-player audio').get(0).paused)
			{
		  		$(".detail .audio-player audio")[0].load();
		  		$('.detail .audio-player audio').get(0).play();
		  		$('.detail .audio-player audio').prop("controls",true);
		  		$('.detail .audio-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$(".detail .audio-player audio")[0].load();
			  	$('.detail .audio-player audio').get(0).pause();
			  	$('.detail .audio-player audio').prop("controls",false);
			  	$('.detail .audio-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .audio-player audio').on('ended',function(){
			  	$('.detail .audio-player audio')[0].load();
			  	$('.detail .audio-player audio').prop("controls",false); 
			  	$('.detail .audio-player .play').fadeIn('slow');
		    });	
		}
	}
})

.controller('MyComplaintsCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $sce) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('My Complaints');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope);
			
			//Send API Call
            API.getComplaintsUser($rootScope.session.iduser)
			.success(function(data) {
				$scope.complaints = data.data;
			})
			.error(function(data) {
				var alertPopup = $ionicPopup.alert({
				     title: 'Incorruptible',
				     template: data.msg
				});
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Go View Complaint
	$scope.goViewComplaint = function(event,idcomplaint) {
    	$state.go('ViewComplaint', { 'complaintId': idcomplaint });
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Date Format
	$scope.formatDate = function(date) {
		var d = new Date(date.replace(' ', 'T'));
		return new Date(d);
	}
	
	//Open Profile
  	$rootScope.goDashboard = function(event) {
	  	$state.go('dashboard');
  	}
})

.controller('ComplaintsDependencyCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $sce) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Complaints Dependency');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope);
			
			//Procesamos el Comentario
			API.getComplaintsResolvedGroupDependency($rootScope.session.iduser)
			.success(function(data) {
				console.log(data);
				$scope.complaints = data.data.complaints;
				
				$ionicLoading.hide();
			})
			.error(function(data) {
				console.log(data);
				$ionicLoading.hide();
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Go View Complaint
	$scope.goViewComplaint = function(event,idcomplaint) {
    	$state.go('ViewComplaint', { 'complaintId': idcomplaint });
	}
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Date Format
	$scope.formatDate = function(date) {
		var d = new Date(date.replace(' ', 'T'));
		return new Date(d);
	}
	
	//Open Profile
  	$rootScope.goDashboard = function(event) {
	  	$state.go('dashboard');
  	}
})

.controller('ViewComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $cordovaFile, $cordovaCapture, $cordovaMedia, $ionicPopup, $ionicModal, $compile) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	}
	
	$ionicModal.fromTemplateUrl('form.html', {
    	scope: $scope,
		animation: 'slide-in-up'
  	}).then(function(modal) {
    	$scope.modal = modal;
  	});
  	$scope.openModal = function() {
    	$scope.modal.show();
  	};
  	$scope.closeModal = function() {
    	$scope.modal.hide();
  	};
  	// Cleanup the modal when we're done with it!
  	$scope.$on('$destroy', function() {
    	$scope.modal.remove();
  	});
  	// Execute action on hide modal
  	$scope.$on('modal.hidden', function() {
    	// Execute action
  	});
  	// Execute action on remove modal
  	$scope.$on('modal.removed', function() {
    	// Execute action
  	});
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('View Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$ionicLoading.show({
		      template: 'Procesando...',
		      duration: 3000
		    }).then(function(){
		       console.log("The loading indicator is now displayed");
		    });
		    
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope.session);
		}
		else
		{
			$rootScope.session = {}
			
			//Redirect Start
			$state.go('welcome');
		}
		
		$scope.isIOS = ionic.Platform.isIOS();
		$scope.isAndroid = ionic.Platform.isAndroid();
		$scope.owner = false;
		
		//Read IdComplaint
		var idComplaint = $stateParams.complaintId;
		
		//Check IdComplaint
		if (idComplaint)
		{
			//Send API Call
	        API.getComplaint(idComplaint)
			.success(function(data) {
				if (data.status)
				{
					$scope.complaint = data.data;
					$scope.statusComplaint = data.data.status;
					if ($rootScope.session.iduser == data.data.iduser) { $scope.owner = true; }
					console.log(data.data);
				}
				else
				{
					console.log(data.msg);
				}
				
			})
			.error(function(data) {
				console.log('error complaints');
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
		
		//Reset Form
		$scope.message = {
		    message : ''
		};
		
		//Reset Form
		$scope.response = {
		    message : ''
		};
		
		//Reset Form
		$scope.notification = {
		    title : '',
		    description : ''
		};
		
		//Create Form
		$scope.form = [{id: '1', name: '', value: ''}];
	});
	
	$scope.addField = function(form) {
		console.log($scope.form);
	};
	
	$scope.addNewChoice = function() {
    	var newItemNo = $scope.form.length+1;
		$scope.form.push({'id' : newItemNo.toString(), 'name' : '', 'value' : ''});
   	};
   
   	$scope.removeNewChoice = function() {
   		var newItemNo = $scope.form.length-1;
   		if ( newItemNo !== 0 ) {
   			$scope.form.pop();
    	}
   	};
   	
   	//updateForm
	$scope.updateForm = function(complaint,item) {
		var valid = true;
		
		//Checamos que tenga valores
		$('input[name^="field_'+item.idmessage+'"]').each(function() {
		    if (!$.trim($(this).val().toString())) { valid = false; }
		});
		
		//Validamos el Formulario
		if (valid)
		{
			var respuestas = [];
			var count = 0; 
			
			//Leemos los Valores
			$('input[name^="field_'+item.idmessage+'"]').each(function() {
				item.json[count].value = $.trim($(this).val().toString());
				count++;
			});
			
			//Convertimos el JSON a Cadena
			var text = JSON.stringify(item.json);
			var idcomplaint = complaint.idcomplaint;
			var owner = $rootScope.session.iduser;
			var idmessage = item.idmessage;
			
			//Actualizamos el Status
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de enviar este formulario a la Autoridad interesada?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Send API Call
					console.log(idcomplaint);
					console.log(text);
					console.log(owner);
			        API.updateMessageComplaint(idcomplaint, text, owner, idmessage)
					.success(function(data) {
						if (data.status)
						{
							//Return to Dashboard
							$state.go('dashboard');
						}
						else
						{
							console.log(data);
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
		}
		else
		{
			//Mostramos el Error
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Debes de llenar todos los campos de texto que solicita la Autoridad para poder enviar el formulario.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
		
		return true;
	}
   	
   	$scope.saveForm = function(complaint,form) {
	   	//Read Values
		var text = '';
		var idcomplaint = complaint.idcomplaint;
		var owner = $rootScope.session.iduser;
		var check = true;
		
		angular.forEach(form, function (value, key) {
			if (check)
			{
				if (!value.name.trim()) { check = false;  }
			}
        });
		
		//Check Values
		if (check)
		{
			//Convertimos el JSON a String
			text = JSON.stringify(form);
			
			//Actualizamos el Status
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de enviar este formulario al creador de la denuncia?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Send API Call
					console.log(idcomplaint);
					console.log(text);
					console.log(owner);
			        API.addMessageComplaint(idcomplaint, text, owner)
					.success(function(data) {
						if (data.status)
						{
							//Actualizamos la data de la denuncia
							$scope.complaint.messages_data = data.data.messages_data;
							
							//Ocultamos el Modal
							$scope.modal.hide();
							
							//Borramos los datos del formulario
							$scope.form = [{id: '1', name: '', value: ''}];
						}
						else
						{
							console.log(data);
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Todos los campos deben tener un nombre del campo, no pueden estar vacíos.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
   	};
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
	
	//Social Share Actions
	$scope.showSocialShareActions = function() {
		$ionicActionSheet.show({
			buttons: [{text: 'Compartir en Twitter'}, {text: 'Compartir en Facebook'}],
			cancelText: 'Cancelar',
			cancel: function() {
				console.log('Cancel Share');
		    },
		    buttonClicked: function (index) {
                if (index === 0) {
                   $scope.shareTwitter();
                }
                else if (index === 1) {
                   $scope.shareFacebook();
                }
 
                //Returning true will close the actionSheet, false will keep it open
                return true;
            }
		});
	}
	
	//Share Twitter Button
	$scope.shareTwitter = function() {
		
		alert('funcionalidad pendiente');
		
	};
	 
	//Share Facebook Button
	$scope.shareFacebook = function() {
		
		
		
	};
	
	// A confirm dialog
	$scope.showConfirmFollow = function(idcomplaint,iduser) {
		var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">Ahora seguiras esta denuncía, te enviaremos una notificación cada vez que allá una actualización.</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				//Send API Call
		        API.followComplaint(idcomplaint,iduser)
				.success(function(data) {
					if (data.status)
					{
						$state.go('dashboard');
					}
					else
					{
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
					
				})
				.error(function(data) {
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
	};
	
	//Join Complaint
	$scope.joinComplaint = function(idcomplaint) {
		
		$state.go('AddCommentComplaint', { 'complaintId': idcomplaint });
			
	};
	
	//Show Media
	$scope.showMedia = function(event) {
		
		if ($scope.complaint.type == 'video')
		{
			//Play Video
			if ($('.detail .video-player video').get(0).paused)
			{
		  		$('.detail .video-player video')[0].load();
		  		$('.detail .video-player video').get(0).play();
		  		$('.detail .video-player video').prop("controls",true);
		  		$('.detail .video-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').get(0).pause();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .video-player video').on('ended',function(){
			  	$('.detail .video-player video')[0].load();
			  	$('.detail .video-player video').prop("controls",false);
			  	$('.detail .video-player .play').fadeIn('slow');
		    });
		}
		
		if ($scope.complaint.type == 'audio')
		{
			//Play Video
			if ($('.detail .audio-player audio').get(0).paused)
			{
		  		$(".detail .audio-player audio")[0].load();
		  		$('.detail .audio-player audio').get(0).play();
		  		$('.detail .audio-player audio').prop("controls",true);
		  		$('.detail .audio-player .play').fadeOut(0);
		  	}
		  	else
		  	{
			  	$(".detail .audio-player audio")[0].load();
			  	$('.detail .audio-player audio').get(0).pause();
			  	$('.detail .audio-player audio').prop("controls",false);
			  	$('.detail .audio-player .play').fadeIn('slow');
		  	}
		  	
		  	//Video Stop
		  	$('.container-detail .detail .audio-player audio').on('ended',function(){
			  	$('.detail .audio-player audio')[0].load();
			  	$('.detail .audio-player audio').prop("controls",false); 
			  	$('.detail .audio-player .play').fadeIn('slow');
		    });	
		}
		
	}
	
	//Share Complaint FB
	$scope.shareComplaintFB = function (complaint) 
	{
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		var page = 'https://app.incorruptible.mx/%23/viewcomplaint/' + complaint.idcomplaint;
		var url = 'https://www.facebook.com/sharer.php?u=' + page;
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'facebook'); 
			return false;
		}
	}
	
	//Share Complaint TW
	$scope.shareComplaintTW = function (complaint) 
	{
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		var page = 'https://app.incorruptible.mx/%23/viewcomplaint/' + complaint.idcomplaint;
		var url = 'http://twitter.com/share?url=' + page + '&text=' + complaint.title;
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'facebook'); 
			return false;
		}
	}
	
	//Date Format
	$scope.formatDate = function(date) {
		var d = new Date(date.replace(' ', 'T'));
		return new Date(d);
	}
	
	//Show Status
	$scope.showStatus = function(status) {
		
		var statusString = '';
		switch (status) {
		    case '0':
		        statusString = "Nueva";
		        break;
		    case '1':
		        statusString = "Válida";
		        break;
		    case '2':
		        statusString = "Agrupada";
		        break;
		    case '3':
		        statusString = "Resuelta";
		        break;
		    case '4':
		        statusString = "Desechada";
		        break;
		    case '5':
		        statusString = "Spam";
		        break;
		    case '6':
		        statusString = "Enviada";
		        break;
		    case '7':
		        statusString = "En Proceso";
		        break;
		    case '8':
		        statusString = "Sin Autoridad";
		        break;
		    case '9':
		        statusString = "No competente";
		        break;
		    default :
		    	statusString = "Por definir";
		        break;
		}
		
		return statusString;
	}
	
	// Add File
	$scope.addFile = function(complaint) {
		$('#fileComplaint').trigger('click');
	}
	
	$scope.onFileSelect = function (files,complaint) {
	    
	    //Show Loading
	    $ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
	    
	    //Read Values
	    var file = files[0];
	    var name = file.name;
	    var size = file.size;
	    var type = file.type.split("/");
	    var mb = parseFloat((size / (1024*1024)).toFixed(2));
	    var limit = 0;
	    if (type[0] == 'video') { limit = 10; } else { limit = 2; }
	    console.log(file);
	    
	    if (mb <= limit)
	    {
		    if (file != undefined)
		    {
			    var filename = file.name;
			    var filesize = file.size;
			    var filetype = file.type;
			    var size = filesize/1000;
			    var type = filetype.split("/");
			    
			    API.uploadFile(files[0],type[0],filename,mb,filetype,complaint.idcomplaint,$rootScope.session.iduser)
				.success(function(data) {
					//Hide Loading
				    $ionicLoading.hide();
					
					//Check Response
					if (data.status)
					{
						//Read Video URL
						$scope.complaint.files_data = data.data;
						console.log(data);
					}
					else
					{
						//Show Alert
					    var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					}
				})
				.error(function(error) {
					console.log(error);
					
					//Hide Loading
				    $ionicLoading.hide();
					
					//Show Alert
				    var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				});
			}
			else
			{
				//Hide Loading
			    $ionicLoading.hide();
			    
				console.log('User cancel upload');
			}
		}
		else
		{
			//Hide Loading
		    $ionicLoading.hide();
		    
		    //Show Alert
		    var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El archivo debe pesar menos de ' + limit + 'MB.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	};
	
	$scope.showFile = function(url) {
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'file'); 
			return false;
		}
	}
	
	$scope.updateSubStatus = function(complaint)
	{
		//Update Complaint Substatus
		complaint.substatus = '0';
	}
	
	$scope.saveComplaintStatus = function(complaint, statusComplaint) 
	{
		//Verificamos que el Status haya cambiado
		//if (complaint.status.toString() != statusComplaint.toString())
		//{
			//Actualizamos el Status
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de cambiar el status de la denuncia?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Send API Call
			        API.changeStatusComplaint(complaint.idcomplaint, complaint.status, complaint.substatus, complaint.public_title, complaint.public_description)
					.success(function(data) {
						if (data.status)
						{
							$state.go('dashboard');
						}
						else
						{
							console.log(data);
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
		//}
	}
	
	$scope.addNotification = function(notification,complaint)
	{
		//Read Values
		var title = notification.title;
		var description = notification.description;
		var idcomplaint = complaint.idcomplaint;
		
		//Check Values
		if (title && description)
		{
			//Actualizamos el Status
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de enviar esta notificación a la denuncia? Esta notificación llegará al autor de la denuncia y a todas las personas que se han unido a ella.</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Send API Call
			        API.addNotificationComplaint(idcomplaint, title, description, $rootScope.session.idprofile)
					.success(function(data) {
						if (data.status)
						{
							//Actualizamos la data de la denuncia
							$scope.complaint = data.data;
							
							//Borramos los datos del formulario
							$scope.notification = {
							    title : '',
							    description : ''
							};
						}
						else
						{
							console.log(data);
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
			
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Escribe un título y descripción de la notificación para la denuncia.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
	$scope.addMessage = function(message,complaint)
	{
		//Read Values
		var text = message.message;
		var idcomplaint = complaint.idcomplaint;
		var owner = $rootScope.session.iduser;
		
		//Check Values
		if (text)
		{
			//Actualizamos el Status
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de enviar este mensaje al creador de la denuncia?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Send API Call
			        API.addMessageComplaint(idcomplaint, text, owner)
					.success(function(data) {
						if (data.status)
						{
							//Actualizamos la data de la denuncia
							$scope.complaint.messages_data = data.data.messages_data;
							
							//Borramos los datos del formulario
							$scope.message = {
							    message : ''
							};
						}
						else
						{
							console.log(data);
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
			
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Escribe un mensaje a enviar...</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
	$scope.addResponseMessage = function(response,complaint)
	{
		//Read Values
		var text = response.message;
		var idcomplaint = complaint.idcomplaint;
		var owner = $rootScope.session.iduser;
		
		//Check Values
		if (text)
		{
			//Actualizamos el Status
			var confirmPopup = $ionicPopup.confirm({
				template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">¿Estás seguro de enviar esta respuesta a la autoridad que esta revisando la denuncia?</div>',
				cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
				cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
	
			confirmPopup.then(function(res) {
				if(res) 
				{
					//Send API Call
			        API.addResponseMessageComplaint(idcomplaint, text, owner)
					.success(function(data) {
						console.log(data);
						if (data.status)
						{
							//Actualizamos la data de la denuncia
							$scope.complaint.messages_data = data.data.messages_data;
							
							//Borramos los datos del formulario
							$scope.response = {
							    message : ''
							};
						}
						else
						{
							//Mostramos el Error
							var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
						
					})
					.error(function(data) {
						//Mostramos el Error
						var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
				} 
				else 
				{
					console.log('You are not sure');
				}
			});
			
		}
		else
		{
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Escribe una respuesta a la autoridad...</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
})

.controller('ViewComplaintFollowCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('View Complaint Follow');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			$scope.showConfirmFollow();
			console.log($rootScope);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Social Share Actions
	$scope.showSocialShareActions = function() {
		$ionicActionSheet.show({
			buttons: [{text: 'Compartir en Twitter'}, {text: 'Compartir en Facebook'}],
			cancelText: 'Cancelar',
			cancel: function() {
				console.log('Cancel Share');
		    },
		    buttonClicked: function (index) {
                if (index === 0) {
                   $scope.shareTwitter();
                }
                else if (index === 1) {
                   $scope.shareFacebook();
                }
 
                //Returning true will close the actionSheet, false will keep it open
                return true;
            }
		});
	}
	
	//Share Twitter Button
	$scope.shareTwitter = function() {
		
		alert('funcionalidad pendiente');
		
	};
	 
	//Share Facebook Button
	$scope.shareFacebook = function() {
		
		alert('funcionalidad pendiente');
		
	};
	
	// A confirm dialog
	$scope.showConfirmFollow = function() {
		var confirmPopup = $ionicPopup.confirm({
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15">Ahora seguiras esta denuncía, te enviaremos una notificación cada vez que allá una actualización.</div>',
			cancelText: 'CANCELAR', // String (default: 'Cancel'). The text of the Cancel button.
			cancelType: 'brown white-text avenir-book', // String (default: 'button-default'). The type of the Cancel button.
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});

		confirmPopup.then(function(res) {
			if(res) 
			{
				console.log('You are sure');
			} 
			else 
			{
				console.log('You are not sure');
			}
		});
	};
	
})

.controller('AddCommentComplaintCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Add Comment Complaint');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			
			//Read IdComplaint
			var idcomplaint = $stateParams.complaintId;
			
			$scope.comment = {
			    value : ''
			};
			
			//Check IdComplaint
			if (idcomplaint)
			{
				//Send API Call
		        API.getComplaintAmounts(idcomplaint)
				.success(function(data) {
					if (data.status)
					{
						$scope.complaint = data.data;
						console.log(data.data);
					}
					else
					{
						console.log(data.msg);
					}
					
				})
				.error(function(data) {
					console.log('error complaints');
				});
			}
			else
			{
				//Redirect Start
				$window.history.back();
			}
		}
		else
		{
			//Redirect Start
			$window.history.back();
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	// A confirm dialog
	$scope.joinComplaint = function() {
		
		var alertPopup = $ionicPopup.alert({
			title: '',
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Comentario publicado.</div></div>',
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});
	
		alertPopup.then(function(res) {
			console.log('Thank you for not eating my delicious ice cream cone');
		});
		
	};
	
	//Select Amount
	$scope.selectAmount = function(event,idamount,comment,complaint) {
		
		//Leemos el ID del Usuario
		var iduser = $rootScope.session.iduser;
		var comment = comment.value;
		
		if (comment)
		{
			//Procesamos el Comentario
			API.joinComplaint(comment,idamount,iduser,complaint.idcomplaint)
			.success(function(data) {
				if (data.status)
				{
					var alertPopup = $ionicPopup.alert({
						title: '',
						template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Te has unido a esta denuncia.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				
					alertPopup.then(function(res) {
						$window.history.back();
					});
				}
				else
				{
					//Mostramos el Error
					var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">'+data.msg+'</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
				}
				
			})
			.error(function(data) {
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
		else
		{
			//Mostramos el Error
			var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Tienes que escribir un comentario para unirte a esta denuncia.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		}
	}
	
})

.controller('AddCommentComplaintJoinCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Add Comment Complaint Join');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			$scope.joinComplaint();
			console.log($rootScope);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	// A confirm dialog
	$scope.joinComplaint = function() {
		
		var alertPopup = $ionicPopup.alert({
			title: 'Don\'t eat that!',
			template: '<div class="table-cell vtop padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.49 55.025"><defs><style>.cls-1{fill:#67a89b;}</style></defs><title>like</title><path class="cls-1" d="M51.885,37.408a6.822,6.822,0,0,0,1.6-4.568,7.01,7.01,0,0,0-1.5-3.958A6.15,6.15,0,0,0,50.527,21.8c-1.83-1.668-4.936-2.416-9.24-2.209a36.128,36.128,0,0,0-5.66.725h-.012c-.576.1-1.186.23-1.807.368-.047-.736.08-2.565,1.438-6.685,1.611-4.9,1.52-8.653-.3-11.162A7.313,7.313,0,0,0,29.09,0a2.89,2.89,0,0,0-2.221,1.012C25.592,2.5,25.742,5.235,25.9,6.5c-1.518,4.073-5.775,14.061-9.377,16.834a1.249,1.249,0,0,0-.186.161,12.332,12.332,0,0,0-2.254,3.383,4.757,4.757,0,0,0-2.279-.576H4.787A4.791,4.791,0,0,0,0,31.091v18.7a4.791,4.791,0,0,0,4.787,4.787h7.02a4.775,4.775,0,0,0,2.762-.875l2.7.322a115.9,115.9,0,0,0,15.339.84c1.369.1,2.658.161,3.855.161a25.808,25.808,0,0,0,5.35-.483c3.522-.748,5.926-2.244,7.147-4.441a7.047,7.047,0,0,0,.782-4.407,7.342,7.342,0,0,0,2.612-5.973A7.791,7.791,0,0,0,51.885,37.408ZM4.787,51.47a1.682,1.682,0,0,1-1.68-1.68V31.079a1.682,1.682,0,0,1,1.68-1.68h7.02a1.682,1.682,0,0,1,1.68,1.68v18.7a1.682,1.682,0,0,1-1.68,1.68H4.787ZM48.94,36.062a1.546,1.546,0,0,0-.207,1.875,4.791,4.791,0,0,1,.529,1.921A4.605,4.605,0,0,1,47.1,43.841a1.568,1.568,0,0,0-.529,1.772,4.061,4.061,0,0,1-.31,2.97c-.77,1.38-2.485,2.37-5.087,2.922a30.265,30.265,0,0,1-8.388.253h-.161a113.5,113.5,0,0,1-14.959-.816h-.012L16.49,50.8a4.923,4.923,0,0,0,.1-1.012V31.079a4.847,4.847,0,0,0-.219-1.427,9.622,9.622,0,0,1,2.141-3.946c5.166-4.1,10.219-17.917,10.436-18.515a1.471,1.471,0,0,0,.069-.771,7.209,7.209,0,0,1,.148-3.337,4.131,4.131,0,0,1,3.246,1.554C33.59,6.259,33.543,9.159,32.277,13c-1.933,5.857-2.094,8.941-.563,10.3a2.51,2.51,0,0,0,2.509.449c.7-.161,1.369-.3,2-.4.046-.012.1-.022.15-.034,3.532-.77,9.861-1.243,12.06.76,1.863,1.7.541,3.958.391,4.2a1.54,1.54,0,0,0,.276,2,4.361,4.361,0,0,1,1.277,2.681A4.275,4.275,0,0,1,48.94,36.062Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Comentario publicado.</div></div>',
			okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
			okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
		});
	
		alertPopup.then(function(res) {
			console.log('Thank you for not eating my delicious ice cream cone');
		});
		
	};
	
})

.controller('BlogCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Blog');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope);
			
			$scope.feed = {};
			
			//Procesamos el Comentario
			API.getBlog()
			.success(function(data) {
				$scope.feed = data.data;
				
				$ionicLoading.hide();
			})
			.error(function(data) {
				$ionicLoading.hide();
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Go to My Complaints
	$scope.goLastComplaints = function(event) {
	  	$state.go('MyComplaints');
  	}
	
	//Go to Ranking Complaints
	$rootScope.goRankingComplaints = function(event) {
	  	console.log('state ranking complaints');
	  	//$state.go('RankingComplaints');
  	}
  	
  	//Go to New Complaint
  	$rootScope.newComplaint = function(event) {
		$state.go('newComplaint');
	}
	
	//Go to Post
	$scope.goPost = function(event,url) {
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'Post'); 
			return false;
		}
		
	}
		
	//Date Format
	$scope.formatDate = function(date) {
		var d = new Date(date);
		return d;
	}
	
	//Open Profile
  	$scope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
})

.controller('NewsCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('News');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
			
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope);
			
			$scope.feed = {};
			
			//Procesamos el Comentario
			API.getNews()
			.success(function(data) {
				$scope.feed = data.data;
				
				$ionicLoading.hide();
			})
			.error(function(data) {
				$ionicLoading.hide();
				//Mostramos el Error
				var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de conexión. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Go to My Complaints
	$rootScope.goLastComplaints = function(event) {
	  	$state.go('MyComplaints');
  	}
	
	//Go to Ranking Complaints
	$rootScope.goRankingComplaints = function(event) {
	  	console.log('state ranking complaints');
	  	//$state.go('RankingComplaints');
  	}
  	
  	//Go to Single News
  	$rootScope.newsSingle = function(event) {
		$state.go('newsSingle');
	}
	
	//Go to Post
	$scope.goPost = function(event,url) {
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			var target = '_blank';
			var options = "location=no,closebuttoncaption=Cerrar";
			var ref = cordova.InAppBrowser.open(url, target, options);
			
			ref.addEventListener('loadstart', loadstartCallback);
			ref.addEventListener('loadstop', loadstopCallback);
			ref.addEventListener('loadloaderror', loaderrorCallback);
			ref.addEventListener('exit', exitCallback);
			
			function loadstartCallback(event) {
			  console.log('Loading started: '  + event.url)
			}
			
			function loadstopCallback(event) {
			  console.log('Loading finished: ' + event.url)
			}
			
			function loaderrorCallback(error) {
			  console.log('Loading error: ' + error.message)
			}
			
			function exitCallback() {
			  console.log('Browser is closed...')
			}
			 
			return false;
		}
		else
		{
			window.open(url, 'Post'); 
			return false;
		}
		
	}
		
	//Date Format
	$scope.formatDate = function(date) {
		var d = new Date(date);
		return d;
	}
	
	//Open Profile
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
		
})

.controller('NewsSingleCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $ionicActionSheet, $sce, $ionicPopup) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('News Single');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope);
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Social Share Actions
	$scope.showSocialShareActions = function() {
		$ionicActionSheet.show({
			buttons: [{text: 'Compartir en Twitter'}, {text: 'Compartir en Facebook'}],
			cancelText: 'Cancelar',
			cancel: function() {
				console.log('Cancel Share');
		    },
		    buttonClicked: function (index) {
                if (index === 0) {
                   $scope.shareTwitter();
                }
                else if (index === 1) {
                   $scope.shareFacebook();
                }
 
                //Returning true will close the actionSheet, false will keep it open
                return true;
            }
		});
	}
	
	//Share Twitter Button
	$scope.shareTwitter = function() {
		
		alert('funcionalidad pendiente');
		
	};
	 
	//Share Facebook Button
	$scope.shareFacebook = function() {
		
		alert('funcionalidad pendiente');
		
	};
		
})

.controller('ProfileCtrl', function($ionicPlatform, $scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $ionicPopup) {

	var uuid;
	
	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
		
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Profile');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope.session);
			
			var image = 'img/user.png';
			
			$scope.perfil = {
			    password : '',
			    state: $rootScope.session.idstate,
			    mailing: ($rootScope.session.mailing === '1'),
			    notifications: ($rootScope.session.notifications === '1'),
			};
			
			API.getStates()
			.success(function(data) {
				$scope.states = data.data;
			})
			.error(function(data) {
				console.log('error states');
			});
			
			$scope.$apply(function() {
				if ($rootScope.session.avatar) { image = $rootScope.session.avatar; }
				$rootScope.newAvatar = '';
				$rootScope.newPassword = '';
				$rootScope.avatar = image;
				$rootScope.iduser = $rootScope.session.iduser;
				$rootScope.name = $rootScope.session.name;
				$rootScope.email = $rootScope.session.email;
				$rootScope.profile = $rootScope.session.profile;
				$rootScope.social = false;
				if ($rootScope.session.fbid) { $rootScope.fbid = true; $rootScope.social = true; }
				else { $rootScope.fbid = false; }
				if ($rootScope.session.twid) { $rootScope.twid = true; $rootScope.social = true; }
				else { $rootScope.twid = false; }
				if ($rootScope.session.idprofile == '1') { $rootScope.menuUser = true; }
				else { $rootScope.menuUser = false; }
				if ( $rootScope.idprofile = '2' ) { $rootScope.ong = true; }
				else { $rootScope.ong = false; }
				if ( $rootScope.idprofile = '3' ) { $rootScope.aut = true; }
				else { $rootScope.aut = false; }
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//Open Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
  	
  	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	};
	
	//Edit Profile
	$scope.goEditProfile = function (event) {
		$state.go('editProfile');
	};
	
	$scope.onImageSelect = function (files) {
	    
	    //Show Loading
	    $ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
	    
	    //Read Values
	    var file = files[0];
	    console.log(file);
	    
	    if (file != undefined)
	    {
		    var filename = file.name;
		    var filesize = file.size;
		    var filetype = file.type;
		    var size = filesize/1000;
		    var type = filetype.split("/");
		    
		    //Check File Size < 2MB
		    if (size < 2048)
		    {
			    //Check File Type
			    if (type[0] == 'image')
			    {
				    API.uploadImage(files[0])
					.success(function(data) {
						//Hide Loading
					    $ionicLoading.hide();
						
						//Check Response
						if (data != 'error')
						{
							//Read Video URL
							$rootScope.newAvatar = data;
							
							//Actualizar Imagen en la vista
							$('.profile .image .avatar').attr('src', data);
						}
						else
						{
							//Show Alert
						    var alertPopup = $ionicPopup.alert({
							    title: 'Incorruptible',
							    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
								okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
								okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
							});
						}
					})
					.error(function(error) {
						console.log(error);
						
						//Hide Loading
					    $ionicLoading.hide();
						
						//Show Alert
					    var alertPopup = $ionicPopup.alert({
						    title: 'Incorruptible',
						    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
							okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
							okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
						});
					});
			    }
			    else
			    {
				    //Hide Loading
				    $ionicLoading.hide();
				    
				    //Show Alert
				    var alertPopup = $ionicPopup.alert({
					    title: 'Incorruptible',
					    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">El archivo seleccionado no es una imagen.</div></div>',
						okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
						okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
					});
			    }
		    }
		    else
		    {
			    //Hide Loading
			    $ionicLoading.hide();
			    
			    //Show Alert
			    var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">La imagen debe pesar menos de 2MB.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
		    }
		}
		else
		{
			console.log('User cancel upload');
		}
	};
	
	//Update Avatar
	$scope.updateAvatar = function (event) {
		
		var isIOS = ionic.Platform.isIOS();
		var isAndroid = ionic.Platform.isAndroid();
		
		if (isIOS || isAndroid)
		{
			if (isIOS)
			{
				$scope.takeImage();
			}
			else
			{
				$('#imageFile').trigger('click');
			}
			
		}
		else
		{
			//Load Image Input Dialog
			$('#imageFile').trigger('click');
		}
		
	};
	
	//Capture Photo
	$scope.takeImage = function() {
		
		$ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
  
		// capture callback
		var captureSuccessImage = function(mediaFiles) {
		    var i, path, len;
		    for (i = 0, len = mediaFiles.length; i < len; i += 1)
		    {
		        path = mediaFiles[i].fullPath;
		        // do something interesting with the file
		        
		        // Destination URL
				var url = "https://api.incorruptible.mx/upload";
			 
				// File for Upload
				var targetPath = path;
				
				// File name only
				var filename = path.replace(/^.*[\\\/]/, '');
			 
				var options = {
			    	fileKey: "file",
					fileName: filename,
					chunkedMode: false,
					mimeType: "multipart/form-data"
				};
				
				var params = new Object();
				params.type = 'image';
				params.filename = 'filename';
				params.filepath = path;
				
				options.params = params;
			 
				$cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
					$ionicLoading.hide();
					if (result.response != 'error')
					{
						//Read Video URL
						$rootScope.newAvatar = data;
						
						//Actualizar Imagen en la vista
						$('.profile .image .avatar').attr('src', data);
					}
					else
					{
						alert('Error');
					}
				}, function(err) {
					$ionicLoading.hide();
					console.log("ERROR: " + JSON.stringify(err));
					alert(JSON.stringify(err));
				}, function (progress) {
					// constant progress updates
					$timeout(function () {
						$scope.downloadProgress = (progress.loaded / progress.total) * 100;
					});
				});
			}
		};
		
		// capture error callback
		var captureErrorImage = function(error) {
		    //navigator.notification.alert('Error code: ' + error.code, null, 'No se capturo nada.');
		    $ionicLoading.hide();
		};
		
		// start image capture
		navigator.device.capture.captureImage(captureSuccessImage, captureErrorImage, {limit:1});
	}
	
	//Update Profile
	$scope.updateProfile = function (perfil) {
		
		//Show Loading
	    $ionicLoading.show({
	    	template: 'Procesando...',
			noBackdrop: false
	    });
	    
		if (perfil != undefined)
		{
			$rootScope.newPassword = perfil.password;
			$rootScope.newState = perfil.state;
			$rootScope.newMailing = perfil.mailing;
			$rootScope.newNotifications = perfil.notifications;
		}
		
		//Procesamos la Actualización
		API.updateProfile($rootScope.session.iduser,$rootScope.newAvatar,$rootScope.newPassword,$rootScope.newState,$rootScope.newMailing,$rootScope.newNotifications)
		.success(function(data) {
			//Hide Loading
		    $ionicLoading.hide();
			
			//Check Response
			if (data.status)
			{
				//Update Session
				$rootScope.session.idstate = data.data.idstate;
				$rootScope.session.state = data.data.state;
				$rootScope.session.position = data.data.position;
				$rootScope.session.mailing = $rootScope.newMailing ? '1' : '0';
				$rootScope.session.notifications = $rootScope.newNotifications ? '1' : '0';
				
				//Verify Avatar Update
				if ($rootScope.newAvatar)
				{
					//Update Avatar at Session
					$rootScope.session.avatar = $rootScope.newAvatar;
				}
				
				//Save New Session Data
				$window.localStorage.setItem("inc_session", JSON.stringify($rootScope.session));
				
				//Go to Complaint Title
				$state.go('dashboard');
			}
			else
			{
				//Show Alert
			    var alertPopup = $ionicPopup.alert({
				    title: 'Incorruptible',
				    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
					okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
					okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
				});
			}
		})
		.error(function(error) {
			console.log(error);
			
			//Hide Loading
		    $ionicLoading.hide();
			
			//Show Alert
		    var alertPopup = $ionicPopup.alert({
			    title: 'Incorruptible',
			    template: '<div class="table-cell vmiddle padding-1rem-sides"><svg style="width:50px;" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25.611 17.44"><defs><style>.cls-1{fill:#d1704c;}</style></defs><title>police</title><path class="cls-1" d="M25.406,5.265a1.164,1.164,0,0,0-.615-1.772L14.241.218a4.827,4.827,0,0,0-2.87,0L.821,3.493A1.164,1.164,0,0,0,.206,5.265L4.425,11.4v1.5c-1.176.464-1.77,1.029-1.77,1.687,0,.884,1.064,1.6,3.165,2.124a30.121,30.121,0,0,0,6.986.726,30.11,30.11,0,0,0,6.984-.726c2.1-.525,3.166-1.24,3.166-2.123,0-.658-.595-1.223-1.77-1.687V11.4Zm-5.049,7.362H5.255v-.94h15.1v.94ZM11.3,5.965l-.617-1.008,1.183.03a.417.417,0,0,0,.375-.216l.565-1.039.564,1.039a.415.415,0,0,0,.375.216l1.183-.03-.617,1.008a.417.417,0,0,0,0,.434l.617,1.009-1.183-.031a.419.419,0,0,0-.375.217l-.564,1.039-.565-1.039a.415.415,0,0,0-.364-.217h-.011l-1.183.031L11.3,6.4A.412.412,0,0,0,11.3,5.965Zm13.244-1.68a.334.334,0,0,1,.177.509l-4.168,6.062H13.22V9.607l.758-1.394,1.691.043a.415.415,0,0,0,.365-.631l-.884-1.443.884-1.443a.416.416,0,0,0-.365-.632l-1.691.043L13.22,2.757V.852a4.032,4.032,0,0,1,.774.16ZM.849,4.5a.33.33,0,0,1,.218-.21l10.55-3.274A3.983,3.983,0,0,1,12.39.852V2.757l-.757,1.394L9.942,4.108a.417.417,0,0,0-.37.207.413.413,0,0,0,.006.424l.883,1.443L9.578,7.626a.414.414,0,0,0,.364.631l1.691-.043.757,1.393v1.25H5.058L.89,4.794A.333.333,0,0,1,.849,4.5Zm21.278,10.1c0,.162-.21.382-.561.59a7.808,7.808,0,0,1-1.977.728,29.31,29.31,0,0,1-6.783.7,29.329,29.329,0,0,1-6.785-.7c-2.006-.5-2.537-1.082-2.537-1.319,0-.278.58-.735,1.873-1.134h14.9C21.547,13.856,22.127,14.313,22.127,14.591Z"/><path class="cls-1" d="M12.806,7.039a.8.8,0,1,0-.8-.8A.8.8,0,0,0,12.806,7.039Z"/></svg></div><div class="table-cell vtop padding-1rem-sides avenir-book font15"><div class="space20">Ocurrió un error de comunicación. Intenta más tarde.</div></div>',
				okText: 'ACEPTAR', // String (default: 'OK'). The text of the OK button.
				okType: 'brown white-text avenir-book', // String (default: 'button-positive'). The type of the OK button.
			});
		});
	}
})

.controller('MyNotificationsCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $compile, $cordovaGeolocation, uiGmapGoogleMapApi, $cordovaCamera, $cordovaFile, $cordovaCapture, $sce) {
	
	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}
	
	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('My Notifications');
        }
		
		if ($window.localStorage.getItem("inc_session"))
		{
			$ionicLoading.show({
		      template: 'Procesando...',
		      duration: 3000
		    }).then(function(){
		       console.log("The loading indicator is now displayed");
		    });
		    
			$rootScope.session = JSON.parse($window.localStorage.getItem("inc_session"));
			console.log($rootScope);
			
			//Send API Call
			API.getNotificationsUser($rootScope.session.iduser)
			.success(function(data) {
				$scope.notifications = data.data;
				console.log(data);
			})
			.error(function(data) {
				var alertPopup = $ionicPopup.alert({
				     title: 'Incorruptible',
				     template: data.msg
				});
			});
		}
		else
		{
			//Redirect Start
			$state.go('welcome');
		}
	});
	
	//seeDetails
	$scope.seeDetails = function(view,identifier,idnotification) {
		
		//Update Notification
		API.readNotification(idnotification)
		.success(function(data) {
			console.log(data);
		})
		.error(function(data) {
			console.log(data);
		});
		
		//Check Identifier
		if (!identifier)
		{
			//Send View
			$state.go(view);
		}
		else
		{
			//View Complaint
			if (view == 'ViewComplaint')
			{
				$state.go('ViewComplaint', { 'complaintId': identifier });
			}
		}
	}
	
	//Go Dashboard
  	$rootScope.gotoDashboard = function(event) {
	  	$state.go('dashboard');
  	}
	
})

.controller('VerifyCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $cordovaDevice, API, $ionicLoading, $timeout, $ionicPopup) {

	var uuid;

	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() {
		uuid = $cordovaDevice.getUUID();
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		
		if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Verify');
        }
        
		if ($window.localStorage.getItem("inc_session"))
		{
			//Redirect To Home App
			$state.go('dashboard');
		}
		
		$scope.result = false;
		$scope.msg = '';
		
		//Read Reset Info
		$scope.$apply(function() {
			
			if ($stateParams.userId) { $scope.iduser = $stateParams.userId; } 
			else { $scope.iduser = ''; }
			
			if ($stateParams.token) { $scope.token = $stateParams.token; }
			else { $scope.token = ''; }
			
			API.verifyUser($scope.iduser,$scope.token)
			.success(function(data) {
				$scope.msg = data.msg;
				if (data.status)
				{
					//Result
					$scope.result = true;
				}
			})
			.error(function(data) {
				console.log('error verify');
			});
		});
	});
	
	//Open Login View
	$scope.login = function(event) {
		$state.go('login');
	}
})

.controller('LogoutCtrl', function($scope, $state, $stateParams, $location, $window, $rootScope, API, $ionicSideMenuDelegate) {

	//Load View
  	$scope.$on('$ionicView.enter', function() {
	  	
	  	if(typeof analytics !== 'undefined') {
            analytics.startTrackerWithId("UA-99925236-1");
            analytics.trackView('Logout');
        }
	  	
		//Check Session
		if ($window.localStorage.getItem("inc_session"))
		{
			//Destroy Session
			$window.localStorage.removeItem("inc_session");
		}
		
		$ionicSideMenuDelegate.toggleLeft();

		//Redirect Start
		$state.go('welcome');
	});
	
})