angular.module('inc.services', [])

.factory('API',['$q','$http','CONFIG', '$window',function($q,$http,CONFIG, $window){

	function createSignature(method, endPoint, headerParameters, bodyParameters, secretKey, tokenSecret) {
        if (typeof jsSHA !== "undefined") {
            var headerAndBodyParameters = angular.copy(headerParameters);
            var bodyParameterKeys = Object.keys(bodyParameters);
            for (var i = 0; i < bodyParameterKeys.length; i++) {
                headerAndBodyParameters[bodyParameterKeys[i]] = escapeSpecialCharacters(bodyParameters[bodyParameterKeys[i]]);
            }
            var signatureBaseString = method + "&" + encodeURIComponent(endPoint) + "&";
            var headerAndBodyParameterKeys = (Object.keys(headerAndBodyParameters)).sort();
            for (i = 0; i < headerAndBodyParameterKeys.length; i++) {
                if (i == headerAndBodyParameterKeys.length - 1) {
                    signatureBaseString += encodeURIComponent(headerAndBodyParameterKeys[i] + "=" + headerAndBodyParameters[headerAndBodyParameterKeys[i]]);
                } else {
                    signatureBaseString += encodeURIComponent(headerAndBodyParameterKeys[i] + "=" + headerAndBodyParameters[headerAndBodyParameterKeys[i]] + "&");
                }
            }
            var oauthSignatureObject = new jsSHA(signatureBaseString, "TEXT");

            var encodedTokenSecret = '';
            if (tokenSecret) {
                encodedTokenSecret = encodeURIComponent(tokenSecret);
            }

            headerParameters.oauth_signature = encodeURIComponent(oauthSignatureObject.getHMAC(encodeURIComponent(secretKey) + "&" + encodedTokenSecret, "TEXT", "SHA-1", "B64"));
            var headerParameterKeys = Object.keys(headerParameters);
            var authorizationHeader = 'OAuth ';
            for (i = 0; i < headerParameterKeys.length; i++) {
                if (i == headerParameterKeys.length - 1) {
                    authorizationHeader += headerParameterKeys[i] + '="' + headerParameters[headerParameterKeys[i]] + '"';
                } else {
                    authorizationHeader += headerParameterKeys[i] + '="' + headerParameters[headerParameterKeys[i]] + '",';
                }
            }
            return {signature_base_string: signatureBaseString, authorization_header: authorizationHeader, signature: headerParameters.oauth_signature};
        } else {
            return "Missing jsSHA JavaScript library";
        }
    }

    function createNonce(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    function escapeSpecialCharacters(string) {
        var tmp = encodeURIComponent(string);
        tmp = tmp.replace(/\!/g, "%21");
        tmp = tmp.replace(/\'/g, "%27");
        tmp = tmp.replace(/\(/g, "%28");
        tmp = tmp.replace(/\)/g, "%29");
        tmp = tmp.replace(/\*/g, "%2A");
        return tmp;
    }

    function transformRequest(obj) {
        var str = [];
        for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + escapeSpecialCharacters(obj[p]));
        console.log(str.join('&'));
        return str.join('&');
    }
    
    var platform = '';
    var isIOS = ionic.Platform.isIOS();
	var isAndroid = ionic.Platform.isAndroid();
	if (isIOS) { platform = 'ios'; }
	if (isAndroid) { platform = 'android'; }
	if (!isIOS && !isAndroid) { platform = 'web'; }
    
    return {
		getBlog: function(){
			var dataObj = {
		        msg : "getBlog",
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
		},
		getNews: function(){
			var dataObj = {
		        msg : "getNews",
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
		},
		uploadVideo: function(file) {
			var fd = new FormData();
	        fd.append('file', file);
	        fd.append('type', 'video');
	        return $http.post('https://api.incorruptible.mx/upload', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        });
		},
		uploadImage: function(file) {
			var fd = new FormData();
	        fd.append('file', file);
	        fd.append('type', 'image');
	        return $http.post('https://api.incorruptible.mx/upload', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        });
		},
		uploadFile: function(file,type,name,size,fulltype,idcomplaint,iduser) {
			var fd = new FormData();
	        fd.append('file', file);
	        fd.append('type', type);
	        fd.append('name', name);
	        fd.append('size', size);
	        fd.append('fulltype', fulltype);
	        fd.append('idcomplaint', idcomplaint);
	        fd.append('iduser', iduser);
	        return $http.post('https://api.incorruptible.mx/upload/file', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        });
		},
		uploadFileNew: function(file,type,name,size,fulltype) {
			var fd = new FormData();
	        fd.append('file', file);
	        fd.append('type', type);
	        fd.append('name', name);
	        fd.append('size', size);
	        fd.append('fulltype', fulltype);
	        return $http.post('https://api.incorruptible.mx/upload/file_new', fd, {
	            transformRequest: angular.identity,
	            headers: {'Content-Type': undefined}
	        });
		},
		getTwitterProfile: function (clientId,clientSecret,token,secret) {

            var deferred = $q.defer();
            var oauthObject = {
                oauth_consumer_key: clientId,
                oauth_nonce: createNonce(10),
                oauth_signature_method: "HMAC-SHA1",
                oauth_token: token,
                oauth_timestamp: Math.round((new Date()).getTime() / 1000.0),
                oauth_version: "1.0"
            };
            var signatureObj = createSignature('GET', 'https://api.twitter.com/1.1/account/verify_credentials.json', oauthObject, { 'include_email' : 'true' }, clientSecret, secret);
            $http.defaults.headers.common.Authorization = signatureObj.authorization_header;
            
            $http({
                method: 'GET',
                url: "https://api.twitter.com/1.1/account/verify_credentials.json",
                params: {'include_email': 'true'},
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (result)
            {
                //console.log(result);
                //alert('USER TIMELINE: ' + JSON.stringify(result));
                
                var dataObj = {
					msg : "loginTwitter",
					fields : {
						email: result.email,
						name: result.screen_name,
						twid: result.id_str,
						twtoken: token,
						avatar: result.profile_image_url.replace('_normal', '')
					},
					platform: platform
				};
				$http.post(CONFIG.API_ENDPOINT, dataObj)
				.success(function(data) {
					if (data.status)
					{
						$window.localStorage.setItem("inc_session", JSON.stringify(data.data));
					}
				})
				.error(function(data) {
					console.log('ERROR API 500');
				});
                
                //deferred.resolve(result);
            }).error(function (error)
            {
                //alert("Error: " + JSON.stringify(error));
                //deferred.reject(false);
                return false;
            });
            //return deferred.promise;
        },
        doLoginTwitter:function(email,name,twid,twtoken,avatar) {
			var dataObj = {
				msg : "loginTwitter",
				fields : {
					email: email,
					name: name,
					twid: twid,
					twtoken: twtoken,
					avatar: avatar
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
		doLogin:function(name,password) {
			var dataObj = {
				msg : "loginUser",
				fields : {
					name: name,
					password: password
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        doRegister:function(email,name,password,idstate) {
			var dataObj = {
				msg : "createUser",
				fields : {
					email: email,
					name: name,
					password: password,
					idstate: idstate
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getCategories: function() {
	        var dataObj = {
		        msg : "getCategories",
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getAmounts: function() {
	        var dataObj = {
		        msg : "getAmounts",
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        saveComplaint: function(complaint,iduser) {
	        var media = '';
	        if (complaint.type == 'video') { media = complaint.video; }
	        if (complaint.type == 'audio') { media = complaint.audio; }
	        if (complaint.type == 'image') { media = complaint.image; }
	        var dataObj = {
		        msg : "saveComplaint",
		        fields : {
			        title : complaint.title,
			        description : complaint.description,
			        lat : complaint.lat,
			        lng : complaint.lng,
			        address : complaint.address,
			        type : complaint.type,
			        media : media,
			        idcategory : complaint.category,
			        idamount : complaint.amount,
			        iduser : iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaints: function(iduser,time,category,amount,type) {
	        var dataObj = {
		        msg : "getComplaints",
		        fields : {
			        iduser : iduser,
			        time : time,
			        category : category,
			        amount : amount,
			        type : type
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaintsONG: function(iduser,time,category,amount,type) {
	        var dataObj = {
		        msg : "getComplaintsONG",
		        fields : {
			        iduser : iduser,
			        time : time,
			        category : category,
			        amount : amount,
			        type : type
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaintsDependency: function(iduser) {
	        var dataObj = {
		        msg : "getComplaintsDependency",
		        fields : {
			        iduser : iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getNewComplaints: function() {
	        var dataObj = {
		        msg : "getNewComplaints",
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaint: function(idComplaint) {
	        var dataObj = {
		        msg : "getComplaint",
		        fields : {
			        idcomplaint : idComplaint
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaintAmounts: function(idComplaint) {
	        var dataObj = {
		        msg : "getComplaintAmounts",
		        fields : {
			        idcomplaint : idComplaint
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        changeStatusComplaint: function(idcomplaint,status,substatus,public_title,public_description) {
	        var dataObj = {
		        msg : "changeStatusComplaint",
		        fields : {
			        idcomplaint : idcomplaint,
			        status : status,
			        substatus : substatus,
			        public_title : public_title,
			        public_description : public_description
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        forgotPassword: function(email) {
	        var dataObj = {
				msg : "forgotPassword",
				fields : {
					email: email
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        resetPassword: function(iduser,token,password,confirm) {
	        var dataObj = {
				msg : "resetPassword",
				fields : {
					iduser: iduser,
					token: token,
					password: password
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        followComplaint: function(idcomplaint,iduser) {
	        var dataObj = {
				msg : "followComplaint",
				fields : {
					idcomplaint: idcomplaint,
					iduser: iduser
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        loginFacebook:function(token) {
			var dataObj = {
				msg : "loginFacebook",
				fields : {
					token: token
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaintsUser: function(iduser) {
	        var dataObj = {
		        msg : "getComplaintsUser",
		        fields : {
			        iduser: iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        joinComplaint: function(comment,idamount,iduser,idcomplaint) {
	        var dataObj = {
				msg : "joinComplaint",
				fields : {
					text: comment,
					idamount: idamount,
					iduser: iduser,
					idcomplaint: idcomplaint
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getDependencies: function() {
	        var dataObj = {
		        msg : "getDependencies",
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getDependenciesByTerm: function(term) {
	        var dataObj = {
		        msg : "getDependenciesByTerm",
		        fields: {
			        term: term
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        saveGroupComplaints: function(title, description, level, dependency, iduser, complaints) {
	        var dataObj = {
				msg : "saveGroupComplaints",
				fields : {
					title: title,
					description: description,
					level: level,
					dependency: dependency,
					iduser: iduser,
					complaints: complaints
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        sendGroup: function(idgroup) {
	        var dataObj = {
		        msg : "sendGroup",
		        fields: {
			        idgroup: idgroup
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        deleteComplaintFromGroup: function(idcomplaint,idgroup,title,idprofile,iduser) {
	        var dataObj = {
		        msg : "deleteComplaintFromGroup",
		        fields: {
			        idcomplaint: idcomplaint,
			        idgroup: idgroup,
			        title: title,
			        idprofile: idprofile,
			        iduser: iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getGroupsByUser: function(iduser) {
	        var dataObj = {
				msg : "getGroupsByUser",
				fields : {
					iduser: iduser
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getGroupsByDependency: function(iduser) {
	        var dataObj = {
				msg : "getGroupsByDependency",
				fields : {
					iduser: iduser
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaintsResolvedGroupDependency: function(iduser) {
	        var dataObj = {
				msg : "getComplaintsResolvedGroupDependency",
				fields : {
					iduser: iduser
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getGroupsResolvedByDependency: function(iduser) {
	        var dataObj = {
				msg : "getGroupsResolvedByDependency",
				fields : {
					iduser: iduser
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getComplaintsGroupDependency: function(idgroup) {
	        var dataObj = {
				msg : "getComplaintsGroupDependency",
				fields : {
					idgroup: idgroup
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getGroup: function(idgroup) {
	        var dataObj = {
				msg : "getGroup",
				fields : {
					idgroup: idgroup
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        updateProfile:function(iduser,avatar,password,idstate,mailing,notifications) {
			var dataObj = {
				msg : "updateProfile",
				fields : {
					iduser: iduser,
					avatar: avatar,
					password: password,
					idstate: idstate,
					mailing: mailing,
					notifications: notifications
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        registerDevice:function(iduser,registrationId,platform) {
			var dataObj = {
				msg : "registerDevice",
				fields : {
					iduser: iduser,
					registrationId: registrationId,
					platform: platform
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getNotificationsUser: function(iduser) {
	        var dataObj = {
		        msg : "getNotificationsUser",
		        fields : {
			        iduser: iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        readNotification: function(idnotification) {
	        var dataObj = {
		        msg : "readNotification",
		        fields : {
			        idnotification: idnotification
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        addNotificationComplaint: function(idcomplaint, title, description,idprofile) {
	        var dataObj = {
		        msg : "addNotificationComplaint",
		        fields : {
			        idcomplaint: idcomplaint,
			        title: title,
			        description: description,
			        idprofile: idprofile
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        addMessageComplaint: function(idcomplaint, text, iduser) {
	        var dataObj = {
		        msg : "addMessageComplaint",
		        fields : {
			        idcomplaint: idcomplaint,
			        text: text,
			        iduser: iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        updateMessageComplaint: function(idcomplaint, text, iduser, idmessage) {
	        var dataObj = {
		        msg : "updateMessageComplaint",
		        fields : {
			        idcomplaint: idcomplaint,
			        text: text,
			        iduser: iduser,
			        idmessage: idmessage
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        addResponseMessageComplaint: function(idcomplaint, text, iduser) {
	        var dataObj = {
		        msg : "addResponseMessageComplaint",
		        fields : {
			        idcomplaint: idcomplaint,
			        text: text,
			        iduser: iduser
		        },
		        platform: platform
	        };
	        return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        verifyUser: function(iduser,token) {
	        var dataObj = {
				msg : "verifyUser",
				fields : {
					iduser: iduser,
					token: token
				},
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
        getStates: function() {
	        var dataObj = {
				msg : "getStates",
		        platform: platform
			};
			return $http.post(CONFIG.API_ENDPOINT, dataObj);
        },
	}

}])

.value('CONFIG',{
    APP_ID: 'yIkjB91C7MHUk4lynlMpOraUnsebIZWjCqiF1XxW',
    REST_API_KEY:'RDTL4E0GEkgND14modyLqNc2x6kGJMvM1sJxLam0',
    API_ENDPOINT:'https://api.incorruptible.mx/v2'
});